###########################################################################################################
CytoASP: A Cytoscape app for qualitative consistency analysis, prediction and repair in biological networks
###########################################################################################################

* Main Developer: Amelie Barozet - Department of Informatics, King's College London,  London, UK.
* Co-developers:
    - Jekaterina Sereshti - Institute of Biometry and Informatics, University of Heidelberg, Heidelberg, Germany.
    - Aristotelis Kittas - Department of Informatics, King's College London,  London, UK.

Description
===========
This app checks the consistency of a network with experimental data. It makes predictions and suggests repairs. Source code for the Cytoscape app jar file is included. Contains the java files and the executable necessary for the app.

Jar Archive
===========
The jar file must include the files present in the bin folder (*.class* files and *exe.linux-x86_64-2.7.zip* file). The manifest file to be used is the app-manifest file present in the main folder.

About the *to_executable* folder
================================
The *to_executable folder* is not to be included in the jar archive. It contains the code necessary for the update of the *exe.linux-x86_64-2.7.zip* file in the bin and src folders (which corresponds to the python part of the plugin, calling BioASP).

To update the *exe.linux-x86_64-2.7.zip* file, the following steps must be followed:

1. Modify the *check_inflgraph_11.py* and *netio.py* as needed to update the function of the executable.
2. Install the *cx-freeze*, *pyasp* and *__ingranalyze__* python packages.
3. Add the following line to *query.py* in *__ingranalyze__*, instead of the default allocation of the variable root:
    .. sourcecode :: python

        root = __file__.rsplit('/', 3)[0] + '/__ingranalyze__'

4. Add the following line to *asp.py*  in *pyasp*, instead of the default allocation of the variable root:
    .. sourcecode :: python

        root = __file__.rsplit('/', 3)[0] + '/pyasp'

5. Go to the *to_executable folder* and run *cx-freeze* to create the executable:
    .. sourcecode :: python

        python setup.py build

6. Create a zip file out of the *exe.linux-x86_64-2.7* folder in the build folder.
7. Replace the existing zip file with the new one.

**Attention** : the name of the zip file must not be changed, or the reference to the zip file in the java code must be updated as well.

Acknowledgement
===============
This app uses the Python script ingranalyze.py_ as a backend to provide BioASP functionality, and therefore uses portion of its code it the packaged files.

.. _ingranalyze.py: http://bioasp.github.io/ingranalyze/

Example
=======
1. A sample network (*yeast_guelzim.net*) and observations file (*yeast_snf2.obs*) is provided in the /samples directory of this repository. The network may be imported directly in Cytoscape. Having the network selected in Cytoscape the user may run CytoASP. Tooltips are provided as the user hovers the mouse over different elements of the interface, to provide explanations about the expected input and output. CytoASP automatically categorises the edge signs, however custom assignment of edges into 'positive', 'negative' or 'unknown' influences is also possible.
2. The user then proceeds to assign the observations to their corresponding nodes. This may be done by:
    i. Importing the observations as node attributes from Cytoscape and selecting them from the list presented to the user.
    ii. Selecting the observations file from CytoASP, which imports and assigns the node observations directly.
3. The user selects whether repair sets or Minimal Inconsistent cores are to be calculated, a repair mode and a folder to output results.
4. The user selects colours for visualising up- and down- regulation status of the nodes.
5. The app executes, presenting information to the user in a console window, in addition to file output.

Note that if multiple networks are selected in Cytoscape, CytoASP proceeds to analyse them in parallel, increasing efficiency of multiple analyses in multi-core CPU architectures.
