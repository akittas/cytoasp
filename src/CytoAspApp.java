import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;
import controller.Controller;
import model.CytoAsp;
import org.cytoscape.app.swing.AbstractCySwingApp;
import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.swing.AbstractCyAction;


/**
 * <b> Class CytoAspApp </p>
 * <p> This Class is common to any simple app in cytoscape. </p>
 * <p> It contains the CySwingAppAdapter that will be necessary 
 * to handle the different cytoscape functionalities in the CytoAsp Class. </p>
 * <p> Its constructor creates an instance of the CytoAsp Class and of the Controller Class
 * and adds the "CytoAsp" option in the App Menu </p>
 * @author 
 *
 */
public class CytoAspApp extends AbstractCySwingApp {
	/**
	 * This field contains the adapter for simple apps that will be used to access Cytoscape functionalities.
	 */
	CySwingAppAdapter adapter;
	
	
	/**
	 * Constructor CytoAspApp
	 * <p>This constructor creates an instance of MyAppMenuAction and registers it into Cytoscape, which adds the "CytoAsp" option under the App menu</p>
	 * @param swingAdapter The adapter for simple apps that will be used to access Cytoscape functionalities.
	 */
	public CytoAspApp(CySwingAppAdapter swingAdapter) {
		super(swingAdapter);
		MyAppMenuAction menuAction = new MyAppMenuAction();
		swingAdapter.getCySwingApplication().addAction(menuAction);
		adapter=swingAdapter;
	}
	
	/**
	 * <b> MyAppMenuAction </b>
	 * <p></p>
	 * This class describes the "CytoAsp" option, it sets the menu that should contain this option (App), and details what has to be done when this option is clicked on.
	 * @author 
	 *
	 */
	public class MyAppMenuAction extends AbstractCyAction {
		private static final long serialVersionUID = 1L;
		
		public MyAppMenuAction() {
			super("CytoAsp");
			setPreferredMenu("Apps");
		}
		public void actionPerformed(ActionEvent e) {
			try {
				CytoAsp ca = new CytoAsp(adapter);
				Controller controller = new Controller(ca);
			} catch (Exception e1) {
				JOptionPane.showMessageDialog(null,
						e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}
}