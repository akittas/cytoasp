package controller;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import gui.EdgesOverview;
import gui.ObservationsPanel;
import gui.OptionsPanel;
import gui.ResultsPanel;
import gui.ColorPanel;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.Timer;
import model.CytoAsp;
/**
 * <b> Class Controller </b>
 * <p> </p>
 * <p> This Class is at the intermediary between the user interface and the CytoAsp Class</p>
 * @author
 *
 */
public class Controller {
	private JFrame window;
	private CytoAsp model;
	private JPanel contentPane;
	private File directory;
	private int repairMode;
	private boolean mics;
	private boolean repairSets, delete;

	/**
	 * Controller Constructor
	 * <p>Creates an instance of Controller. 
	 * Creates the dialogue window used to display the different panels that will be used to choose the options
	 * and fills it with the first Panel : EdgeOverview</p>
	 * @param m
	 */
	public Controller(CytoAsp m) {
		super();
		this.window = new JFrame();
		this.model = m;
		window.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {//the user asked to close the window
				if (model.isCalculating()) {//the calculation is ongoing
					int answer = JOptionPane.showConfirmDialog(
							window,
							"Do you want to exit CytoASP? Analysis will be stopped.",
							"Exit",
							JOptionPane.YES_NO_OPTION);
					if (answer == JOptionPane.OK_OPTION) {//the user chose to close the window anyway
						try {
							model.stopCalculating();
							window.dispose();
						} catch (IOException e1) {
							JOptionPane.showMessageDialog(window,"Error while stopping python", "Error",JOptionPane.ERROR_MESSAGE);
						}
					}
				} 
				else//no calculation ongoing, we don't need a confirmation to close the window
					window.dispose();
			}
		});
		window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		window.setBounds(100, 100, 650, 360);
		contentPane = new EdgesOverview(new InteractionListener(), model);
		window.setContentPane(contentPane);
		window.setVisible(true);
	}

	private class InteractionListener extends SwingWorker<String, String>
	implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();

			//-------------------------------------------------
			//The command received is InteractionsFinished
			//-------------------------------------------------
			if (command.equalsIgnoreCase("InteractionsFinished")) {
				//we replace the EdgeOverview by the ObservationsPanel
				setNewPanelVisible(new ObservationsPanel(model, this));
			}
			//-------------------------------------------------
			//The command received is obsFinished
			//-------------------------------------------------
			else if (command.equalsIgnoreCase("obsFinished")) {
				//we must replace the ObservationsPanel by the OptionsPanel
				contentPane = (JPanel) window.getContentPane();
				ObservationsPanel panel = (ObservationsPanel) contentPane;
				if (panel.isNodeSelected()) {//The user chose the "Observation from attribute" option
					String attribute = panel.getChosenNode();
					if (attribute == null || attribute.isEmpty()) 
						JOptionPane.showMessageDialog(panel,"No attribute is chosen","Error",JOptionPane.ERROR_MESSAGE);
					else {
						if (model.setObservationsFromAttribute(attribute)) {
							//the attribute chosen is ok, the observations have been recorded
							setNewPanelVisible(new OptionsPanel(this));
						} 
						else {
							// there was something wrong with the attribute selected
							JOptionPane.showMessageDialog(
									panel,
									"The chosen attribute does not exist for all selected networks or has wrong format: String, Integer and Floating are allowed",
									"Error",
									JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				else {//The user chose to get the observations from a file
					File f = panel.getChosenFile();
					if (f != null) {
						model.setObservationsFile(f);
						setNewPanelVisible(new OptionsPanel(this));
					} 
					else {
						//no file chosen
						JOptionPane
						.showMessageDialog(
								panel,
								"Please choose file/node attribute with the observations",
								"Error",
								JOptionPane.ERROR_MESSAGE);
					}
				}
			} 
			//-------------------------------------------------
			//The command received is optionsFinished
			//-------------------------------------------------
			else if (command.equalsIgnoreCase("optionsFinished")) {
				contentPane = (JPanel) window.getContentPane();
				OptionsPanel panel = (OptionsPanel) contentPane;
				directory = panel.getChosenDirectory();
				repairMode = panel.getRepairMode();
				mics = panel.getMics();
				delete = panel.getDelete();
				repairSets = panel.getRepairSets();
				setNewPanelVisible(new ColorPanel(this,repairMode));
			} 
			//-------------------------------------------------
			//The command received is calculate
			//-------------------------------------------------
			else if (command.equalsIgnoreCase("calculate")) {
				contentPane = (JPanel) window.getContentPane();
				ColorPanel panel = (ColorPanel) contentPane;
				//communicating the colours
				model.setNodeBorderNeg(panel.getNodeBorderNeg());
				model.setNodeBorderPos(panel.getNodeBorderPos());
				model.setNodeColorNeg(panel.getNodeColorNeg());
				model.setNodeColorPos(panel.getNodeColorPos());
				model.setEdgesPos(panel.getEdgesPos());
				model.setEdgesNeg(panel.getEdgesNeg());
				model.setInputs(panel.getInputs());

				setNewPanelVisible(new ResultsPanel());
				try {
					execute();//calls the doInBackground and done functions
				} 
				catch (Exception e1) {
					JOptionPane.showMessageDialog(
							window,
							"Error while analysing the network: "
									+ e1.getMessage()+"  " + e1.toString());
				}
			} 
			//-------------------------------------------------
			//The command received is toEdgePanel
			//-------------------------------------------------
			else if (command.equalsIgnoreCase("toEdgePanel")) {
				setNewPanelVisible(new EdgesOverview(this, model));
			} 
			//-------------------------------------------------
			//The command received is toObservations
			//-------------------------------------------------
			else if (command.equalsIgnoreCase("toObservations")) {
				setNewPanelVisible(new ObservationsPanel(model, this));
			}
			//-------------------------------------------------
			//The command received is toOptions
			//-------------------------------------------------
			else if (command.equalsIgnoreCase("toOptions")) {
				setNewPanelVisible(new OptionsPanel(this));
			}

		}
		/**
		 * Replaces the current panel in the window with the new one (e.g. when one of the previous or next button is clicked on)
		 * @param newPanel : the new panel which is going to replace the previous one
		 */
		private void setNewPanelVisible(JPanel newPanel) {
			contentPane = (JPanel) window.getContentPane();
			contentPane.setVisible(false);
			contentPane.setEnabled(false);
			window.setContentPane(newPanel);
			contentPane.setVisible(true);
			contentPane.setEnabled(true);
			window.validate();
			window.setVisible(true);
		}
		/* (non-Javadoc)
		 * function called when the command "calculate" is received. 
		 * It returns a String containing the information following the execution of the python scripts by model.calculate.
		 * @see javax.swing.SwingWorker#doInBackground()
		 */
		@Override
		protected String doInBackground() throws Exception {
			return model.calculate(directory, repairMode, mics,
					repairSets,
					delete);
		}
		/* (non-Javadoc)
		 * function called after the doInBackground is done. It displays the information abour the execution (results, failures, in the result panel
		 * and replaces the optionPanel with the result panel.
		 * @see javax.swing.SwingWorker#done()
		 */
		@Override
		protected void done() {
			contentPane = (JPanel) window.getContentPane();
			ResultsPanel panel = (ResultsPanel) contentPane;
			try {
				//get() is the result of the model.calculate function. Here we try to display the different information obtained
				panel.setText("\n" + get());
			} catch (Exception e) {
				//the model.calculate function didn't succeed
				JOptionPane.showMessageDialog(window,
						"Error while analysing the network: " +
								e.getMessage() );
			}

			panel.setText("Finished");
			panel.setText("Autoclosing window in 20 seconds. Type C to cancel autoclosing: ");
			
			final Timer timer = new Timer(20000, new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					window.setVisible(false);
					window.dispose();
				}
			});
			timer.setRepeats(false);
			panel.setKeyListener(new KeyListener() {
				@Override
				public void keyTyped(KeyEvent arg0) {
					if (arg0.getKeyChar() == 'c' || arg0.getKeyChar()== 'C') {
						timer.stop();
					}
				}
				@Override
				public void keyReleased(KeyEvent arg0) {
				}
				@Override
				public void keyPressed(KeyEvent arg0) {
				}
			});
			timer.start();
		}
	}
}