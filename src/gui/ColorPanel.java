package gui;




import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import model.CytoAsp;
/**
 * <b> Class ColorPanel </b>
 * <p>This class defines and creates the ColorPanel, 
 * which is the panel obtained when clicking "next" on the OptionsPanel.
 * It contains the different options in term of colors, according to the repair options chosen by the user..</p>
 * @author 
 *
 */
public class ColorPanel extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	//these colors will be passed to the model so that the new colors chosen by the user can be added to cytoscape
	private Color nodeBorderPos=Color.GREEN, nodeBorderNeg=Color.RED, nodeColorPos=Color.GREEN, nodeColorNeg=Color.RED, edgesPos=Color.GREEN, edgesNeg=Color.RED, inputs=Color.BLUE;
	
	//the following buttons are used and their background color are modified by the listener.
	private JButton borderPosButton, borderNegButton, colorPosButton, colorNegButton, edgesPosButton, edgesNegButton, removeEdgesPos, removeEdgesNeg, inputsButton;
	/**
	 * Creates the OptionsPanel.
	 * @param listener : An actionListener which will react to the interaction with the Next and Finish buttons, and which is defined in Controller.
	 */
	public ColorPanel(ActionListener listener, int repairMode) {
		//setting layout
		GridBagLayout gridBagLayout = new GridBagLayout();
		setLayout(gridBagLayout);


		//finish button
		JButton okButton = new JButton("Finish");
		okButton.setActionCommand("calculate");
		okButton.addActionListener(listener);
		GridBagConstraints constrains = new GridBagConstraints();

		constrains.gridx = 3;
		constrains.anchor = GridBagConstraints.LINE_END;
		constrains.gridy = 7;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 0, 10, 10);
		add(okButton, constrains);

		//previous button
		JButton prevButton = new JButton("Previous");
		prevButton.setActionCommand("toOptions");
		prevButton.addActionListener(listener);
		constrains = new GridBagConstraints();
		constrains.gridx = 0;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 7;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(prevButton, constrains);

		//listener for color changing
		Listener l = new Listener();


		//PREDICTED NODES
		JLabel predictedNodes=new JLabel("Set the nodes border color for predictions");
		constrains = new GridBagConstraints();
		constrains.gridx = 0;
		constrains.anchor = GridBagConstraints.CENTER;
		constrains.gridy =0 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.gridwidth = 4;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(predictedNodes,constrains);

		//Button to change the border color of positively-predicted nodes
		JLabel borderPosLabel=new JLabel("Color for nodes PREDICTED + :");
		borderPosButton = new JButton("Change");
		borderPosButton.setBackground(nodeBorderPos);
		borderPosButton.setActionCommand("changeBorderPos");
		borderPosButton.addActionListener(l);
		constrains = new GridBagConstraints();
		constrains.gridx = 0;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 1 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(borderPosLabel,constrains);
		constrains = new GridBagConstraints();
		constrains.gridx = 1;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 1 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(borderPosButton,constrains);

		//Button to change the border color of negatively-predicted nodes
		JLabel borderNegLabel=new JLabel("Color for nodes PREDICTED - :");
		borderNegButton = new JButton("Change");
		borderNegButton.setBackground(nodeBorderNeg);
		borderNegButton.setActionCommand("changeBorderNeg");
		borderNegButton.addActionListener(l);
		constrains = new GridBagConstraints();
		constrains.gridx = 2;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 1 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(borderNegLabel,constrains);
		constrains = new GridBagConstraints();
		constrains.gridx = 3;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 1 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(borderNegButton,constrains);



		//OBSERVED NODES
		JLabel observedNodes=new JLabel("Set the nodes inside color for observations");
		constrains = new GridBagConstraints();
		constrains.gridx = 0;
		constrains.anchor = GridBagConstraints.CENTER;
		constrains.gridy = 2 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.gridwidth = 4;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(observedNodes,constrains);



		//Button to change the inside color of positively-observed nodes
		JLabel colorPosLabel=new JLabel("Color for nodes OBSERVED + :");
		colorPosButton = new JButton("Change");
		colorPosButton.setBackground(nodeColorPos);
		colorPosButton.setActionCommand("changeColorPos");
		colorPosButton.addActionListener(l);
		constrains = new GridBagConstraints();
		constrains.gridx = 0;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 3 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(colorPosLabel,constrains);
		constrains = new GridBagConstraints();
		constrains.gridx = 1;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 3 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(colorPosButton,constrains);

		//Button to change the inside color of negatively-observed nodes
		JLabel colorNegLabel=new JLabel("Color for nodes OBSERVED - :");
		colorNegButton = new JButton("Change");
		colorNegButton.setBackground(nodeColorNeg);
		colorNegButton.setActionCommand("changeColorNeg");
		colorNegButton.addActionListener(l);
		constrains = new GridBagConstraints();
		constrains.gridx = 2;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 3 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(colorNegLabel,constrains);
		constrains = new GridBagConstraints();
		constrains.gridx = 3;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy = 3 ;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(colorNegButton,constrains);

		if (repairMode==2)//if repair mode is flip influences, we need the user to choose colors for edges which will be flipped.
		{
			//INTERACTION
			JLabel interactionsLabel=new JLabel("Set the edges color according to repair predictions");
			constrains = new GridBagConstraints();
			constrains.gridx = 0;
			constrains.anchor = GridBagConstraints.CENTER;
			constrains.gridy = 4 ;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.gridwidth = 4;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(interactionsLabel,constrains);


			//Button to change color of positive interactions
			JLabel edgesPosLabel=new JLabel("Color for interactions predicted + :");
			edgesPosButton = new JButton("Change");
			edgesPosButton.setBackground(edgesPos);
			edgesPosButton.setActionCommand("edgesPos");
			edgesPosButton.addActionListener(l);
			constrains = new GridBagConstraints();
			constrains.gridx = 0;
			constrains.anchor = GridBagConstraints.LINE_START;
			constrains.gridy = 5 ;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(edgesPosLabel,constrains);
			constrains = new GridBagConstraints();
			constrains.gridx = 1;
			constrains.anchor = GridBagConstraints.LINE_START;
			constrains.gridy = 5 ;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(edgesPosButton,constrains);
			removeEdgesPos=new JButton("Remove");
			removeEdgesPos.setActionCommand("removeEdgesPos");
			removeEdgesPos.addActionListener(l);
			constrains = new GridBagConstraints();
			constrains.gridx = 2;
			constrains.anchor = GridBagConstraints.CENTER;
			constrains.gridy = 5 ;
			constrains.gridwidth = 2;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(removeEdgesPos,constrains);


			//Button to change the color of negative interactions
			JLabel edgesNegLabel=new JLabel("Color for interactions predicted - :");
			edgesNegButton = new JButton("Change");
			edgesNegButton.setBackground(edgesNeg);
			edgesNegButton.setActionCommand("edgesNeg");
			edgesNegButton.addActionListener(l);
			constrains = new GridBagConstraints();
			constrains.gridx = 0;
			constrains.anchor = GridBagConstraints.LINE_START;
			constrains.gridy = 6 ;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(edgesNegLabel,constrains);
			constrains = new GridBagConstraints();
			constrains.gridx = 1;
			constrains.anchor = GridBagConstraints.LINE_START;
			constrains.gridy = 6 ;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(edgesNegButton,constrains);
			removeEdgesNeg=new JButton("Remove");
			removeEdgesNeg.setActionCommand("removeEdgesNeg");
			removeEdgesNeg.addActionListener(l);
			constrains = new GridBagConstraints();
			constrains.gridx = 2;
			constrains.anchor = GridBagConstraints.CENTER;
			constrains.gridy = 6 ;
			constrains.gridwidth = 2;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(removeEdgesNeg,constrains);
		}
		if (repairMode==3){
			// if repair mode is make node inputs, we need the user to choose a color for node predicted inputs
			JLabel inputLabel=new JLabel("Set the border color for nodes predicted as inputs");
			constrains = new GridBagConstraints();
			constrains.gridx = 0;
			constrains.anchor = GridBagConstraints.CENTER;
			constrains.gridy = 4 ;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.gridwidth = 4;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(inputLabel,constrains);
			//Button to change color of input nodes
			JLabel inputsButtonLabel=new JLabel("Border color for nodes predicted as inputs:");
			inputsButton = new JButton("Change");
			inputsButton.setBackground(inputs);
			inputsButton.setActionCommand("inputs");
			inputsButton.addActionListener(l);
			constrains = new GridBagConstraints();
			constrains.gridx = 0;
			constrains.anchor = GridBagConstraints.LINE_START;
			constrains.gridy = 5 ;
			constrains.gridwidth = 2;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(inputsButtonLabel,constrains);
			constrains = new GridBagConstraints();
			constrains.gridx = 2;
			constrains.gridwidth = 2;
			constrains.anchor = GridBagConstraints.LINE_START;
			constrains.gridy = 5 ;
			constrains.weightx = 1.0;
			constrains.weighty = 1.0;
			constrains.insets = new Insets(10, 10, 10, 10);
			add(inputsButton,constrains);

		}

	}

	private void setNodeBorderPos(Color c){
		nodeBorderPos=c;
	}
	private void setNodeBorderNeg(Color c){
		nodeBorderNeg=c;
	}
	private void setNodeColorPos(Color c){
		nodeColorPos=c;
	}
	private void setNodeColorNeg(Color c){
		nodeColorNeg=c;
	}
	private void setEdgesPos(Color c){
		edgesPos=c;
	}
	private void setEdgesNeg(Color c){
		edgesNeg=c;
	}
	private void setInputs(Color c){
		inputs=c;
	}


	public Color getNodeBorderPos(){
		return nodeBorderPos;
	}
	public Color getNodeBorderNeg(){
		return nodeBorderNeg;
	}
	public Color getNodeColorPos(){
		return nodeColorPos;
	}
	public Color getNodeColorNeg(){
		return nodeColorNeg;
	}
	public Color getEdgesPos(){
		return edgesPos;
	}
	public Color getEdgesNeg(){
		return edgesNeg;
	}
	public Color getInputs(){
		return inputs;
	}





	/**
	 * the listener will deal will all the buttons of color choices. It will record any change made by the user 
	 * and will update the color of the button when a new color is changed.
	 *
	 */
	private class Listener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String command = arg0.getActionCommand();
			if(command.equalsIgnoreCase("changeBorderPos")){
				Color newColor = JColorChooser.showDialog(
						ColorPanel.this,
						"Choose Border Color for nodes predicted +",
						nodeBorderPos);
				if (newColor != null) {
					setNodeBorderPos(newColor);
					borderPosButton.setBackground(nodeBorderPos);
				}
			}
			if(command.equalsIgnoreCase("changeBorderNeg")){
				Color newColor = JColorChooser.showDialog(
						ColorPanel.this,
						"Choose Border Color for nodes predicted -",
						nodeBorderNeg);
				if (newColor != null) {
					setNodeBorderNeg(newColor);
					borderNegButton.setBackground(nodeBorderNeg);
				}
			}
			if(command.equalsIgnoreCase("changeColorPos")){
				Color newColor = JColorChooser.showDialog(
						ColorPanel.this,
						"Choose Inside Color for nodes observed +",
						nodeColorPos);
				if (newColor != null) {
					setNodeColorPos(newColor);
					colorPosButton.setBackground(nodeColorPos);
				}
			}
			if(command.equalsIgnoreCase("changeColorNeg")){
				Color newColor = JColorChooser.showDialog(
						ColorPanel.this,
						"Choose Inside Color for nodes observed -",
						nodeColorNeg);
				if (newColor != null) {
					setNodeColorNeg(newColor);
					colorNegButton.setBackground(nodeColorNeg);
				}
			}
			if(command.equalsIgnoreCase("edgesPos")){
				Color newColor = JColorChooser.showDialog(
						ColorPanel.this,
						"Choose Color for influences predicted +",
						edgesPos);
				if (newColor != null) {
					setEdgesPos(newColor);
					edgesPosButton.setBackground(edgesPos);
					edgesPosButton.setText("change");
					removeEdgesPos.setEnabled(true);
				}
			}
			if(command.equalsIgnoreCase("edgesNeg")){
				Color newColor = JColorChooser.showDialog(
						ColorPanel.this,
						"Choose Color for influences predicted -",
						edgesNeg);
				if (newColor != null) {
					setEdgesNeg(newColor);
					edgesNegButton.setBackground(edgesNeg);
					edgesNegButton.setText("change");
					removeEdgesNeg.setEnabled(true);
				}
			}
			if(command.equalsIgnoreCase("removeEdgesPos")){
				setEdgesPos(null);
				edgesPosButton.setBackground(null);
				edgesPosButton.setText("Define...");
				removeEdgesPos.setEnabled(false);
			}
			if(command.equalsIgnoreCase("removeEdgesNeg")){
				setEdgesNeg(null);
				edgesNegButton.setBackground(null);
				edgesNegButton.setText("Define...");
				removeEdgesNeg.setEnabled(false);
			}
			if(command.equalsIgnoreCase("inputs")){
				Color newColor = JColorChooser.showDialog(
						ColorPanel.this,
						"Choose Border Color for nodes predicted as inputs",
						inputs);
				if (newColor != null) {
					setInputs(newColor);
					inputsButton.setBackground(inputs);
				}
			}
		}
	}
}

