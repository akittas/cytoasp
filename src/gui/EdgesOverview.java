package gui;
import javax.swing.JPanel;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Map;
import java.util.Vector;
import javax.swing.JList;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import model.CytoAsp;
/**
 * <b> Class EdgesOverview </b>
 * <p>This class defines and creates the EdgeOverview Panel, 
 * which is the panel that is directly displayed when CytoAsp is clicked in the 'App' Menu.</p>
 * @author 
 *
 */
public class EdgesOverview extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private CytoAsp ca;
	private String selectedInteraction = "";
	JList<String> listPositive, listNegative, list;
	/**
	 * Creates the EdgesOverview Panel.
	 * @param listener : An actionListener which will react to the interaction with the Next button, and which is defined in Controller.
	 * @param ca : The CytoAsp model, containing all the information on the network, nodes and edges.
	 *
	 */
	public EdgesOverview(ActionListener listener, CytoAsp ca) {
		this.ca = ca;
		JButton nextButton = new JButton("Next");
		nextButton.setActionCommand("InteractionsFinished");
		nextButton.addActionListener(listener);
		JButton positive = new JButton("+");
		positive.setActionCommand("setInteractionPositive");
		positive.setToolTipText("Click here to classify the selected interaction as a positive interaction.");
		positive.addActionListener(new Listener());
		JButton unknown = new JButton("?");
		unknown.setActionCommand("setInteractionUnknown");
		unknown.setToolTipText("Click here to classify the selected interaction as an unknown interaction.");
		unknown.addActionListener(new Listener());
		JButton negative = new JButton("-");
		negative.setActionCommand("setInteractionNegative");
		negative.setToolTipText("Click here to classify the selected interaction as a negative interaction.");
		negative.addActionListener(new Listener());
		Vector<Vector<String>> data = getInteractionVectors();
		//-----------------------------------------
		//creates the list of positive interactions
		//-----------------------------------------
		listPositive = new JList<String>(createListModelwithData(data.elementAt(0)));
		listPositive.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listPositive.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent ep) {
				if (!ep.getValueIsAdjusting()
						&& !listPositive.isSelectionEmpty()) {
					selectedInteraction = (String) listPositive
							.getSelectedValue();
					list.clearSelection();
					listNegative.clearSelection();
				}
			}
		});
		listPositive.setBorder(UIManager.getBorder("List.focusCellHighlightBorder"));
		//-----------------------------------------
		//creates the list of negative interactions
		//-----------------------------------------
		listNegative = new JList<String>(createListModelwithData(data.elementAt(1)));
		listNegative.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listNegative.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent en) {
				if (!en.getValueIsAdjusting()
						&& !listNegative.isSelectionEmpty()) {
					selectedInteraction = (String) listNegative
							.getSelectedValue();
					listPositive.clearSelection();
					list.clearSelection();
				}
			}
		});
		listNegative.setBorder(UIManager.getBorder("List.focusCellHighlightBorder"));
		//-----------------------------------------
		//creates the list of unknown interactions
		//-----------------------------------------
		list = new JList<String>(createListModelwithData(data.elementAt(2)));
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setToolTipText("<html>Choose interaction from the list and <br>click +," +
				" if the interaction is activator,<br> or -, if the interaction " +
				"is inhibitor</html>");
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (!e.getValueIsAdjusting() && !
						list.isSelectionEmpty()) {
					selectedInteraction = (String)
							list.getSelectedValue();
					listPositive.clearSelection();
					listNegative.clearSelection();
				}
			}
		});
		list.setBorder(UIManager.getBorder("List.focusCellHighlightBorder"));
		//-----------------------------------------
		//creates the labels and sets the sizes of the different boxes
		//-----------------------------------------
		JLabel lblEdgeInteractions = new JLabel("Edge Interactions Signs");
		lblEdgeInteractions.setHorizontalAlignment(SwingConstants.CENTER);
		lblEdgeInteractions.setToolTipText("<html>Press a button to assign" +
				" <br>the selected interaction <br>to the corresponding sign</html>");
		JLabel labelAll = new JLabel("Unknown interactions");
		JLabel labelPos = new JLabel("Positive interactions");
		JLabel labelNeg = new JLabel("Negative interactions");
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 0;
		constraints.insets = new Insets(10, 10, 10, 10);
		add(lblEdgeInteractions, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 1;
		add(labelAll, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 2;
		constraints.gridy = 1;
		add(labelPos, constraints);
		constraints = new GridBagConstraints();
		constraints.gridy = 2;
		constraints.gridheight = 8;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(0, 10, 0, 0);
		add(new JScrollPane(list), constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 2;
		constraints.gridy = 2;
		constraints.gridheight = 3;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(0, 0, 0, 10);
		add(new JScrollPane(listPositive), constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 3;
		constraints.anchor=GridBagConstraints.LINE_END;
		add(positive, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 2;
		constraints.gridy = 5;
		constraints.insets = new Insets(10, 10, 0, 0);
		add(labelNeg, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 2;
		constraints.gridy = 6;
		constraints.gridheight = 3;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.insets = new Insets(0, 0, 0, 10);
		add(new JScrollPane(listNegative), constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 7;
		constraints.anchor=GridBagConstraints.LINE_END;
		add(negative, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 3;
		constraints.anchor=GridBagConstraints.LINE_START;
		add(unknown, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 2;
		constraints.gridy = 10;
		constraints.anchor = GridBagConstraints.LINE_END;
		constraints.insets = new Insets(0, 0, 10, 10);
		add(nextButton, constraints);
	}
	private class Listener implements ActionListener {
		// method called when one of the'+', '-', or '?' buttons is clicked
		@Override
		public void actionPerformed(ActionEvent e) {
			//----------------------------------------------------
			//No interaction chosen / edge selected
			//----------------------------------------------------
			if (selectedInteraction.isEmpty() ||
					(list.getSelectedIndex()<0 &&
							listNegative.getSelectedIndex()<0 && 
							listPositive.getSelectedIndex()<0) ) {
				JOptionPane.showMessageDialog(EdgesOverview.this,
						"No interaction is chosen", "Error",
						JOptionPane.ERROR_MESSAGE);
			} 
			else {
				String command = e.getActionCommand();
				DefaultListModel<String> listModel = (DefaultListModel<String>) list.getModel();
				listModel.removeElement(selectedInteraction);
				listModel = (DefaultListModel<String>) listPositive.getModel();
				listModel.removeElement(selectedInteraction);
				listModel = (DefaultListModel<String>) listNegative.getModel();
				listModel.removeElement(selectedInteraction);
				//----------------------------------------------------
				//Command : setInteractionPositive
				//----------------------------------------------------
				if (command.equalsIgnoreCase("setInteractionPositive"))
				{
					ca.setInteraction(selectedInteraction, '+');
					listModel = (DefaultListModel<String>) listPositive.getModel();
				} 
				//----------------------------------------------------
				//Command : setInteractionNegative
				//----------------------------------------------------
				else if (command.equalsIgnoreCase("setInteractionNegative")) {
					ca.setInteraction(selectedInteraction, '-');
					listModel = (DefaultListModel<String>) listNegative.getModel();
				} 
				//----------------------------------------------------
				//Command : setInteractionUnknown
				//----------------------------------------------------
				else if(command.equalsIgnoreCase("setInteractionUnknown")){
					ca.setInteraction(selectedInteraction, '?');
					listModel= (DefaultListModel<String>) list.getModel();
				}
				listModel.addElement(selectedInteraction);
				EdgesOverview.this.validate();
				EdgesOverview.this.repaint();
			}
		}
	}
	
	/**
	 * Populates a list (e.g. the lists of positive, negative, or unknown interactions) with the data provided
	 * @param data : A Vector<String> containing the names of interaction types found in the model
	 * @return A DefaultListModel<String> populated, containing the names of interactions types found in data
	 */
	private DefaultListModel<String> createListModelwithData (Vector<String> data){
		if (data==null)
			throw new RuntimeException("Error while creating List with interactions");
		DefaultListModel<String> model = new DefaultListModel<String>();
		for(String s: data){
			model.addElement(s);
		}
		return model;
	}
	/**
	 * Populates a Vector of String vectors, data, with the interactions given by model.getInteractionsMap()
	 * @return A Vector of String vectors containing the different interactions and their temporary classification between +, - and ?
	 */
	private Vector<Vector<String>> getInteractionVectors(){
		Map<String, Character> interactions = ca.getInteractionsMap();
		Vector<Vector<String>> data = new Vector<Vector<String>>();
		Vector<String> positive, negative, unknown;
		positive = new Vector<String>();
		negative= new Vector<String>();
		unknown = new Vector<String>();
		for(Map.Entry<String, Character> e: interactions.entrySet()){
			if(e.getValue() == '+')  {
				positive.add(e.getKey());
				ca.setInteraction(e.getKey(), e.getValue());
			}
			else if (e.getValue() == '-') {
				negative.add(e.getKey());
				ca.setInteraction(e.getKey(), e.getValue());
			}
			else {
				unknown.add(e.getKey());
				ca.setInteraction(e.getKey(), '?');
			}
		}
		data.add(positive);
		data.add(negative);
		data.add(unknown);
		return data;
	}
}