package gui;
import java.awt.GridBagConstraints;

import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import model.CytoAsp;

/**
 * <b> Class ObservationsPanel </b>
 * <p>This class defines and creates the ObservationsPanel, 
 * which is the panel obtained when clicking "next" on the EdgeOverviewPanel.</p>
 * @author 
 *
 */
public class ObservationsPanel extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private JList<String> nodeAttributesList;
	private JButton changeButton;
	private JLabel chosenFileLabel;
	private File chosenFile =null;
	private JRadioButton nodesRadioButton;

	/**
	 * Creates the ObservationsPanel.
	 * @param model : The CytoAsp model, containing all the information on the network, nodes and edges
	 * @param listener : An actionListener which will react to the interaction with the Next and Previous buttons, and which is defined in Controller
	 */
	public ObservationsPanel(CytoAsp model, ActionListener listener) {
		//button Next, listener handled by Controller
		JButton okButton = new JButton("Next");
		okButton.setActionCommand("obsFinished");
		okButton.addActionListener(listener);
		//button Previous, listener handled by Controller
		JButton prevButton = new JButton("Previous");
		prevButton.setActionCommand("toEdgePanel");
		prevButton.addActionListener(listener);
		//button Change File, listener handled in THIS class
		changeButton = new JButton("Change file");
		changeButton.setEnabled(false);
		changeButton.setVisible(false);
		changeButton.setActionCommand("change");
		Listener l = new Listener();
		changeButton.addActionListener(l);
		JLabel obsLabel = new JLabel("Observations");
		obsLabel.setToolTipText("<html>Choose where<br> the observations are saved</html>");
		JRadioButton fileRadioButton = new JRadioButton("Use observations from a file ", false);
		fileRadioButton.setFocusable(false);
		fileRadioButton.setToolTipText("<html>Observation file should " +
				"have<br>following format:<br>&lt;nodename&gt; = + or<br>&lt;nodename&gt; = - " +
				"or<br>&lt;nodename&gt;&lt;tab&gt;+ or<br>&lt;nodename&gt;&lt;tab&gt;- <br>First " +
				"line is <br>name of experiment<br></html>");
		chosenFileLabel = new JLabel("");
		//radio buttons controlling the choice between observations from a file or from an attribute
		nodesRadioButton = new JRadioButton("Use node attribute ", false);
		nodesRadioButton.setFocusable(false);
		nodesRadioButton.addActionListener(l);
		fileRadioButton.addActionListener(l);
		nodesRadioButton.setActionCommand("node");
		fileRadioButton.setActionCommand("file");
		ButtonGroup group = new ButtonGroup();
		group.add(nodesRadioButton);
		group.add(fileRadioButton);
		//list of available node attributes
		nodeAttributesList = new JList<String>(model.getNodeAttributes());
		nodeAttributesList.setEnabled(false);
		nodesRadioButton.setToolTipText("<html>Choose the attribute<br> " +
				"where the observations are saved. <br>The attribute can be of type <br>Integer, " +
				"String or Floating. <br>For String attributes following values are " +
				"possible:<br> +/-, up/down, positive/negative.</html>");
		//layout
		setLayout(new GridBagLayout());
		GridBagConstraints constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 0;
		constraints.gridwidth = 2;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.insets = new Insets(10, 10, 10, 10);
		add(obsLabel, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 1;
		add (fileRadioButton, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 2;
		add (chosenFileLabel, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 2;
		constraints.anchor=GridBagConstraints.LINE_START;
		add (changeButton, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 3;
		constraints.anchor = GridBagConstraints.LINE_START;
		add (nodesRadioButton, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 3;
		constraints.gridheight = 3;
		constraints.fill = GridBagConstraints.BOTH;
		constraints.ipadx = 100;
		constraints.ipady = 100;
		constraints.weightx = 1.0;
		constraints.weighty = 1.0;
		constraints.insets = new Insets(10, 0, 10, 10);
		add (new JScrollPane(nodeAttributesList), constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 1;
		constraints.gridy = 6;
		constraints.anchor = GridBagConstraints.LINE_END;
		constraints.insets = new Insets(0, 0, 10, 10);
		add (okButton, constraints);
		constraints = new GridBagConstraints();
		constraints.gridx = 0;
		constraints.gridy = 6;
		constraints.anchor = GridBagConstraints.LINE_START;
		constraints.insets = new Insets(0, 0, 10, 10);
		add (prevButton, constraints);
	}
	/**
	 * <b> Class Listener </b>
	 * <p> Handles what happens when one of the button is clicked in the ObservationsPanel, 
	 * except for the next and previous buttons </p>
	 * @author 
	 *
	 */
	private class Listener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			String command = e.getActionCommand();
			if(command.equalsIgnoreCase("node")){
				changeButton.setEnabled(false);
				changeButton.setVisible(false);
				chosenFileLabel.setVisible(false);
				nodeAttributesList.setEnabled(true);
			} else if(command.equalsIgnoreCase("file")){
				if(chosenFile == null) chooseFile();
				if(chosenFile != null){
					changeButton.setEnabled(true);
					changeButton.setVisible(true);
					chosenFileLabel.setVisible(true);
					nodeAttributesList.clearSelection();
					nodeAttributesList.setEnabled(false);
				} else{
					nodesRadioButton.setSelected(true);
					nodeAttributesList.setEnabled(true);
				}
			} else if(command.equalsIgnoreCase("change")){
				chooseFile();
			}
		}
		/**
		 * This function opens a FileChooser window from which the user can 
		 * choose a file where the observations are stored.
		 */
		private void chooseFile(){
			JFileChooser fc = new JFileChooser();
			int returnVal = fc.showOpenDialog(ObservationsPanel.this);
			if(returnVal == JFileChooser.APPROVE_OPTION){
				chosenFile = fc.getSelectedFile();
				chosenFileLabel.setText(chosenFile.getName());
			}
		}
	}
	
	/**
	 * @return The File of observations chosen
	 */
	public File getChosenFile(){
		return chosenFile;
	}
	/**
	 * @return True if the user selected the option "observations from attribute", false otherwise.
	 */
	public boolean isNodeSelected(){
		return nodesRadioButton.isSelected();
	}
	/**
	 * @return The name of the attribute chosen by the user to get the observations from.
	 */
	public String getChosenNode(){
		return nodeAttributesList.getSelectedValue();
	}
}