package gui;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
/**
 * <b> Class OptionsPanel </b>
 * <p>This class defines and creates the OptionsPanel, 
 * which is the panel obtained when clicking "next" on the ObservationsPanel.
 * It contains the different options in term of repair and resolution.</p>
 * @author 
 *
 */
public class OptionsPanel extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private ButtonGroup repairGroup;
	private JCheckBox mics, repairSets, deleteDir;
	private File dir;
	private JLabel chosenDir ;
	/**
	 * Creates the OptionsPanel.
	 * @param listener : An actionListener which will react to the interaction with the Next and Finish buttons, and which is defined in Controller.
	 */
	public OptionsPanel(ActionListener listener) {
		JLabel optionsLabel = new JLabel("Options");
		JLabel repairMode = new JLabel("Repair mode");
		repairMode.setToolTipText("<html>Choose the repair mode<br> for the " +
				"case the network is inconsistent</html>");
		JRadioButton flipVar = new JRadioButton("flip observed variations");
		flipVar.setToolTipText("<html>Select this option to repair by revising experimental observations." +
				" <br>It puts the dataset into question in case of inconsistency " +
				"<br>and may help to identify aberrant measurements.</html>");
		flipVar.setActionCommand("1");
		JRadioButton flipInfl = new JRadioButton("flip influences");
		flipInfl.setActionCommand("2");
		flipInfl.setToolTipText("<html>Select this option to repair by flipping edge signs." +
				"<br> It means that in some experiment " +
				"<br>the effect of some regulators (activator or inhibitor) " +
				"<br>should be corrected.</html>");

		JRadioButton nodesInput = new JRadioButton("define nodes as input");
		nodesInput.setActionCommand("3");
		nodesInput.setToolTipText("<html>Select this option to repair by adding inputs nodes to the model." +
				"<br>Turning a node into an input can be used to indicate missing (unknown) " +
				"<br>regulations or oscillations of regulator.</html>");
		
		/* We choose to remove this option because it is redundant with the previous one in case of a unique set of observations
		JRadioButton varInput = new JRadioButton("define observed variations as input");

		varInput.setActionCommand("4");
		varInput.setToolTipText("<html>Select this option to repair by adding observations as inputs." +
				"<br>Turning an observation into an input can be used to indicate missing (unknown) " +
				"<br>regulations or oscillations of regulator for this experiment." +
				"</html>");
		*/
		JRadioButton addInfl = new JRadioButton("add influences");
		addInfl.setActionCommand("5");
		addInfl.setToolTipText("<html>Select this option to repair by adding new edges. " +
				"<br>It makes sense when the model is incomplete.</html>");
		flipVar.setSelected(true);
		repairGroup = new ButtonGroup();
		repairGroup.add(addInfl);
		//repairGroup.add(varInput);
		repairGroup.add(nodesInput);
		repairGroup.add(flipInfl);
		repairGroup.add(flipVar);
		mics = new JCheckBox("Compute minimal inconsistent cores");
		mics.setToolTipText("<html>Choose whether <br>minimal inconsistent " +
				"cores are to be computed. <br>Computation can take a long time <br>in case of " +
				"large-scale networks.</html>");
		mics.setSelected(false);
		repairSets = new JCheckBox("Compute repair sets");
		repairSets.setToolTipText("<html>Choose whether <br>repair sets are" +
				"to be computed</html>");
		repairSets.setSelected(true);// this option is ticked by default.
		deleteDir = new JCheckBox("Delete temp folder");
		deleteDir.setToolTipText("<html>Choose whether <br>the temp folder " +
				"is to be deleted<br> after the calculation.</html>");
		deleteDir.setSelected(true);
		dir = new File(System.getProperty("user.home"));
		JLabel directory = new JLabel("Folder where results will be " +
				"saved:");
		chosenDir = new JLabel(dir.getAbsolutePath());
		JButton changeButton = new JButton("Change folder");
		changeButton.setActionCommand("change");
		Listener l = new Listener();
		changeButton.addActionListener(l);
		JButton okButton = new JButton("Next");
		okButton.setActionCommand("optionsFinished");
		okButton.addActionListener(listener);
		JButton prevButton = new JButton("Previous");
		prevButton.setActionCommand("toObservations");
		prevButton.addActionListener(listener);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 56, 0};
		setLayout(gridBagLayout);
		GridBagConstraints constrains = new GridBagConstraints();
		constrains.insets = new Insets(0, 10, 0, 10);
		constrains.fill = GridBagConstraints.HORIZONTAL;
		constrains.gridx = 1;
		constrains.gridy=0;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		add(optionsLabel, constrains);
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 0;
		constrains.gridy=1;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 0, 0);
		add(repairMode, constrains);
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 0;
		constrains.gridy=2;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 0, 0);
		add(flipVar, constrains);
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 0;
		constrains.gridy=3;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 0, 0);
		add(flipInfl, constrains);
		
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 0;
		constrains.gridy=4;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 0, 0);
		add(nodesInput, constrains);
		
		/*
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 0;
		constrains.gridy=4;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 0, 0);
		add(varInput, constrains);
		*/
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 0;
		constrains.gridy=5;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 0, 0);
		add(addInfl, constrains);
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 2;
		constrains.gridy=2;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		add(mics, constrains);
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 2;
		constrains.gridy=4;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		add(repairSets, constrains);
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 2;
		constrains.gridy=6;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		add(deleteDir, constrains);
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 0;
		constrains.gridy=7;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 0, 0);
		add(directory, constrains);
		constrains = new GridBagConstraints();
		constrains.gridx = 0;
		constrains.gridy=8;
		constrains.gridwidth = 2;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.fill = GridBagConstraints.HORIZONTAL;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 0, 0);
		add(chosenDir, constrains);
		constrains = new GridBagConstraints();
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridx = 0;
		constrains.gridy=9;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 10, 10, 0);
		add(changeButton, constrains);
		constrains = new GridBagConstraints();
		constrains.gridx = 2;
		constrains.anchor = GridBagConstraints.LINE_END;
		constrains.gridy=10;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(0, 0, 10, 10);
		add(okButton, constrains);
		constrains = new GridBagConstraints();
		constrains.gridx = 0;
		constrains.anchor = GridBagConstraints.LINE_START;
		constrains.gridy=10;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.insets = new Insets(10, 10, 10, 10);
		add(prevButton, constrains);
	}
	/**
	 * @return The directory chosen by the user to write the different result files.
	 */
	public File getChosenDirectory(){
		return dir;
	}
	/**
	 * @return The number corresponding to the RepairMode chosen.
	 */
	public int getRepairMode(){
		String command = repairGroup.getSelection().getActionCommand();
		return Integer.parseInt(command);
	}
	
	/**
	 * @return True if the user asked to compute the minimal inconsistent cores, false otherwise.
	 */
	public boolean getMics(){
		return mics.isSelected();
	}
	/**
	 * @return True if the user ticked the box to delete the temp files, false otherwise.
	 */
	public boolean getDelete(){
		return deleteDir.isSelected();
	}
	/**
	 * @return True if the user chose to compute the repairSets, false otherwise.
	 */
	public boolean getRepairSets(){
		return repairSets.isSelected();
	}
	/**
	 * <b> Class Listener </b>
	 * <p> Handles what happens when the "change folder" button is clicked </p>
	 * @author 
	 *
	 */
	private class Listener implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent arg0) {
			String command = arg0.getActionCommand();
			if(command.equalsIgnoreCase("change")){
				JFileChooser fc = new JFileChooser();
				fc.setSelectedFile(dir);
				fc.setMultiSelectionEnabled(false);
				fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				int returnVal = fc.showOpenDialog(OptionsPanel.this);
				if(returnVal == JFileChooser.APPROVE_OPTION){
					dir = fc.getSelectedFile();
					chosenDir.setText(dir.getAbsolutePath());
				}
			}
		}
	}
}

