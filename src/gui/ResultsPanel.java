package gui;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyListener;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
/**
 * <b> Class ResultsPanel </b>
 * <p>This class defines and creates the ResultsPanel, 
 * which is the panel obtained when clicking "finish" on the OptionsPanel.
 * It contains the different information about the program execution and the results if the execution went fine.</p>
 * @author 
 *
 */
public class ResultsPanel extends JPanel {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private JTextArea results;
	/**
	 * Creates the panel.
	 */
	public ResultsPanel() {
		results = new JTextArea("Calculating...");
		setLayout(new GridBagLayout());
		GridBagConstraints constrains = new GridBagConstraints();
		constrains.gridx = 0;
		constrains.gridy = 0;
		constrains.fill = GridBagConstraints.BOTH;
		constrains.weightx = 1.0;
		constrains.weighty = 1.0;
		constrains.anchor = GridBagConstraints.LINE_START;
		add(new JScrollPane(results), constrains);
	}
	public void setText(String text){
		results.setText(results.getText()+"\n"+text);
	}
	public void setKeyListener(KeyListener listener){
		results.addKeyListener(listener);
	}
}
