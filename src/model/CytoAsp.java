package model;
import java.awt.Color;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.Vector;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.io.CyFileFilter;
import org.cytoscape.io.write.CyNetworkViewWriterManager;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.VisualLexicon;
import org.cytoscape.view.presentation.property.ArrowShapeVisualProperty;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;



/**
 * <b>CytoAsp Class</b>
 * <p> Class that handles the functionalities directly linked to Cytoscape </p>
 * <p> Apart from the CytoAspApp Class, this is the only class that uses the Cytoscape function and classes. </p>
 * <p> </p>
 * <p> It retrieves the different information about the networks, edges and nodes and the tables</p>
 * <p> It gathers this information and sums it up ro prepare the python script execution</p>
 * <p> It executes the python scripts that use BioAsp to compute consistency and the different repairs </p>
 * <p> It uses the results obtained to change the nodes style and make the predictions and observations visible.
 * 
 * 
 * @author 
 *
 */
public class CytoAsp {

	private Set<CytoAspSingleNet> nets;
	public static final String PATH_TO_EXE = "/exe.linux-x86_64-2.7.zip";
	private Map<String, Character> interactionsMap, defaultInteractions, obsMap;
	private File obsFile = null;
	public static final String NEW_ATTRIBUTE_EDGE_SIGN ="CytoASPInteraction";
	public static final String NEW_ATTRIBUTE_PREDICTION ="CytoASPPrediction";
	public static final String NEW_ATTRIBUTE_OBSERVATION = "CytoASPObs";
	private File directory, zipExe;
	private int repairMode;
	private boolean mics, repairSets;
	private boolean calculating = false, stopped = false;
	private ExecutorService pool;
	private CySwingAppAdapter adapter;
	private Color nodeBorderPos, nodeBorderNeg, nodeColorPos, nodeColorNeg, edgesPos, edgesNeg, inputs;


	/**
	 * Constructor CytoAsp
	 * <p>
	 * <p> Initialises the nets, defaultInteractions, interactionsMap, obsMap and adapter fields. </p>
	 * <p> Fills in defaultInteractions and obsMap with the different String attributes understood by Cytoscape. </p>
	 * <p> Creates the NEW_ATTRIBUTE_PREDICTION, NEW_ATTRIBUTE_OBSERVATION and NEW_ATTRIBUTE_EDGE_SIGN 
	 * columns of attributes if they don't exist already. </p>
	 * <p> Calls mapInteractions() at the end.</p>
	 * @param adapter : the CySwingAppAdapter used by the simple app CytoAspApp
	 */
	public CytoAsp(CySwingAppAdapter adapter) {
		//gets the list of networks chosen
		Set<CyNetwork> allNets = adapter.getCyNetworkManager().getNetworkSet();
		if (allNets.isEmpty())
			throw new IllegalArgumentException("No network is chosen");
		nets = new HashSet<CytoAsp.CytoAspSingleNet>();
		// creates a HashSet of CytoAspSingleNets out of the CyNetwork set
		for (CyNetwork net : allNets){
			if (net.getRow(net).get("selected", Boolean.class)){
				nets.add(new CytoAspSingleNet(net,adapter));
			}	
		}

		this.adapter=adapter;
		this.interactionsMap = new TreeMap<String, Character>();
		this.obsMap = new TreeMap<String, Character>();
		this.defaultInteractions = new TreeMap<String, Character>();

		//sets the different String attributes that make sense to CytoAspApp 
		defaultInteractions.put("agent", '+');
		defaultInteractions.put("output", '+');
		defaultInteractions.put("positiveCond", '+');
		defaultInteractions.put("isCompMemberOf", '+');
		defaultInteractions.put("input", '+');
		defaultInteractions.put("isFamMemberOf", '+');
		defaultInteractions.put("hasPathway", '+');
		defaultInteractions.put("inhibitor", '-');
		defaultInteractions.put("+", '+');
		defaultInteractions.put("-", '-');
		defaultInteractions.put("activates", '+');
		defaultInteractions.put("activate", '+');
		defaultInteractions.put("inhibits", '-');
		defaultInteractions.put("inhibit", '-');
		obsMap.put("+", '+');
		obsMap.put("-", '-');
		obsMap.put("up", '+');
		obsMap.put("down", '-');
		obsMap.put("positive", '+');
		obsMap.put("negative", '-');

		//creates the new attributes columns
		for (CyNetwork net : allNets) {
			CyTable edgeTable=net.getDefaultEdgeTable();
			CyTable nodeTable=net.getDefaultNodeTable();
			try {
				edgeTable.createColumn(NEW_ATTRIBUTE_PREDICTION, String.class, false);
			}
			catch (IllegalArgumentException e){
				System.out.println("the columns of attributes have already been created, probably because CytoASP has already been used on this network.");
				System.out.println("overwriting...");
			}
			try {
				nodeTable.createColumn(NEW_ATTRIBUTE_PREDICTION, String.class, false);
			}
			catch (IllegalArgumentException e){
			}
			try {
				nodeTable.createColumn(NEW_ATTRIBUTE_OBSERVATION, String.class, false);
			}
			catch (IllegalArgumentException e){
			}
			try {
				edgeTable.createColumn(NEW_ATTRIBUTE_EDGE_SIGN, String.class, false);
			}
			catch (IllegalArgumentException e){
			}
		}
		mapInteractions();
	}

	/**
	 * Adds to the interactionsMap the different interactions found in the network (under the edges attributes column "interaction").
	 * <p> If the nature of the interaction is already understood by Cytoscape 
	 * (interaction in defaultInteraction or NEW_ATTRIBUTE_EDGE_SIGN already set by a previous use of CytoAspApp)
	 * this function adds it to interactionsMap with the correct sign '+' or '-'. </p>
	 * <p> If not, or if contradictory information exist, the interaction is added with the sign '?' 
	 * and the column CytoASPInteraction contains '?' instead of the previous contradictions </p>
	 */
	private void mapInteractions() {
		boolean labelsMissing=false;
		Set<CyNetwork> allNets = adapter.getCyNetworkManager().getNetworkSet();
		for (CyNetwork net : allNets) {
			if (net.getRow(net).get("selected", Boolean.class)){ 
				List<CyEdge> edgeList=net.getEdgeList();
				String interactionValue;
				for (CyEdge edge : edgeList) {
					interactionValue=net.getRow(edge).get("interaction",String.class);
					String NAES=net.getRow(edge).get(NEW_ATTRIBUTE_EDGE_SIGN,String.class);
					if (interactionValue==null || interactionValue.equals("")) {
						labelsMissing=true;
					}
					else if (NAES!=null && !(NAES.equals(""))) {
						// this NEW_ATTRIBUTE_EDGE_SIGN of edge is known
						if (interactionsMap.containsKey(interactionValue)) {
							if (interactionsMap.get(interactionValue)!=NAES.trim().charAt(0)) {
								// there is a contradiction
								interactionsMap.put(interactionValue, '?');
								setInteraction(interactionValue,'?');
							}
						}
						else {
							interactionsMap.put(interactionValue,NAES.trim().charAt(0));
						}
					}
					else if (interactionsMap.containsKey(interactionValue)) {
						net.getRow(edge).set(NEW_ATTRIBUTE_EDGE_SIGN,interactionsMap.get(interactionValue).toString());
					}
					else {
						if (!defaultInteractions.containsKey(interactionValue))
							interactionsMap.put(interactionValue, '?');
						else
							interactionsMap.put(interactionValue,
									defaultInteractions.get(interactionValue));
					}
				}
			}
		}
		//some interactions are not labelled, empty attribute interaction
		if (labelsMissing) JOptionPane.showMessageDialog(new JFrame(),"some edge interactions are not labelled, ignoring them..."  );
	}

	/**
	 * This function returns a String array containing the different node attributes 
	 * (i.e. the different column names in the nodes table)
	 * @return String array containing the different node attributes
	 */
	public String[] getNodeAttributes() {
		HashSet<String> attributes=new HashSet<String>();
		for (CytoAspSingleNet net : nets){
			CyNetwork network=net.getNet();
			CyTable t=network.getDefaultNodeTable();
			Set<String> attrNames=CyTableUtil.getColumnNames(t);
			attributes.addAll(attrNames);
		}
		return attributes.toArray(new String[0]);
	}


	/**
	 * This function returns the content of the interactionsMap field
	 * @return interactionsMap
	 */
	public Map<String, Character> getInteractionsMap() {
		return interactionsMap;
	}

	/**
	 * This function classifies the edge attribute column whose name is given by the String interaction parameter 
	 * as a positive, negative or unknown interaction type according to the argument sign.
	 * 
	 * @param interaction : the name of the column of edge attributes to classify
	 * @param sign : '+', '-' or '?' : the type of interaction to assign to this column
	 */
	public void setInteraction(String interaction, Character sign) {
		if (sign != '+' && sign != '-' && sign != '?')
			throw new RuntimeException("Error while setting interaction");
		interactionsMap.put(interaction, sign);
		Set<CyNetwork> allNets = adapter.getCyNetworkManager().getNetworkSet();

		for (CyNetwork net : allNets) {
			if (net.getRow(net).get("selected", Boolean.class)){
				List<CyEdge> edgeList=net.getEdgeList();
				String interactionValue;
				for (CyEdge edge : edgeList) {
					interactionValue=net.getRow(edge).get("interaction",String.class);
					if (interaction.equals(interactionValue)){
						net.getRow(edge).set(NEW_ATTRIBUTE_EDGE_SIGN,sign.toString());
					}
				}
			}
		}
	}


	/**
	 * This function takes the name of a column of node attributes
	 * and will fill the observation column according to this attribute.
	 * <p>
	 * If the column of attributes given is a column of Integer, Long or Double, 
	 * each node will be marked present if this attribute is positive, absent if this attribute is negative, unobserved if it doesn't have such an attribute </p>
	 * <p> If the column given is a column of String, 
	 * each node will me marked according to the signification of the string referenced in obsMap
	 * </p>
	 * @param attr : the name of the column giving the observations
	 * @return true if the column attribute was valid, false otherwise
	 */
	public boolean setObservationsFromAttribute(String attr) {
		CyNetwork net;
		String obs="";
		for (CytoAspSingleNet a : nets) {
			net = a.getNet();
			CyColumn attrColumn=net.getDefaultNodeTable().getColumn(attr);
			if (attrColumn==null) return false; // no column with this name
			Class<?> typeAttr = attrColumn.getType();
			List<CyNode> nodeList = net.getNodeList();
			obs= attr + "\n";
			if (String.class.equals(typeAttr)) {
				for (CyNode node : nodeList) {
					String value = net.getRow(node).get(attr, String.class);
					if (value != null) {
						if (obsMap.containsKey(value.toLowerCase())) {
							obs += removeLines(net.getRow(node).get(CyNetwork.NAME, String.class))
									+ " = "
									+ obsMap.get(value.toLowerCase()) + "\n";
						}
					}
				}
			}
			else if (Double.class.equals(typeAttr)){
				for (CyNode node : nodeList) {
					Double value = net.getRow(node).get(attr, Double.class);
					if (value != null) {
						if (value > 0)
							obs += removeLines(net.getRow(node).get(CyNetwork.NAME, String.class)) + " = +\n";
						else if (value < 0)
							obs += removeLines(net.getRow(node).get(CyNetwork.NAME, String.class)) + " = -\n";
					}
				}
			}
			else if (Integer.class.equals(typeAttr)){
				for (CyNode node : nodeList) {
					Integer value = net.getRow(node).get(attr, Integer.class);
					if (value != null) {
						if (value > 0)
							obs += removeLines(net.getRow(node).get(CyNetwork.NAME, String.class)) + " = +\n";
						else if (value < 0)
							obs += removeLines(net.getRow(node).get(CyNetwork.NAME, String.class)) + " = -\n";
					}
				}
			}
			else if (Long.class.equals(typeAttr)){
				for (CyNode node : nodeList) {
					Long value = net.getRow(node).get(attr, Long.class);
					if (value != null) {
						if (value > 0)
							obs += removeLines(net.getRow(node).get(CyNetwork.NAME, String.class)) + " = +\n";
						else if (value < 0)
							obs += removeLines(net.getRow(node).get(CyNetwork.NAME, String.class)) + " = -\n";
					}
				}
			}
			else return false;
			a.setObservations(obs);
		}
		return true;
	}


	/**
	 * Sets the obsFile field
	 * @param obsFile the File containing the observations found in the network
	 */
	public void setObservationsFile(File obsFile) {
		if (obsFile == null)
			throw new RuntimeException("The observation file could not be set");
		this.obsFile = obsFile;
	}

	/**
	 * <p>Function called when pressing the "finish" button</p>
	 * <p>It executes the python scripts on each network with the options provided.</p>
	 * @param dir : the File containing the destination of the different files.
	 * @param repairMode : the repairMode chosen.
	 * @param mics
	 * @param repairSets : true if the user asked to compute the different repair Sets, false otherwise
	 * @param delete : true if the "delete temp" box is ticked, false otherwise
	 * @return The different information on execution and results. Will be written in the result panel.
	 * @throws Exception
	 */
	public String calculate(File dir, int repairMode, boolean mics,
			boolean repairSets, boolean delete) throws Exception {
		this.directory = dir;


		for (CytoAspSingleNet a : nets) {
			a.setObsFile(obsFile);
		}

		String result = "";
		File exe_dir=new File(directory.getAbsolutePath()+File.separatorChar+"check_inflgraph_11");
		if (exe_dir.exists()){
			deleteDirectory(exe_dir);
		}
		System.out.print("Copying zip file and extracting it...");
		extractAndUnzip(directory, PATH_TO_EXE, "check_inflgraph_11.zip");
		System.out.println("    done.");

		this.repairMode = repairMode;
		this.mics = mics;
		this.repairSets = repairSets;
		calculating = true;
		pool = Executors.newFixedThreadPool(nets.size());
		List<Future<String>> resultSet = pool.invokeAll(nets);

		for (Future<String> future : resultSet) {
			result += future.get() + "\n";
		}
		exe_dir=new File(directory.getAbsolutePath()+File.separatorChar+"check_inflgraph_11");
		if (exe_dir.exists()){
			deleteDirectory(exe_dir);
		}
		if (zipExe.exists())
			zipExe.delete();
		calculating = false;
		pool.shutdown();
		if (!stopped) {
			setVisualStyle();
			if (delete) {
				File tempDir = new File(directory.getAbsoluteFile()
						+ File.separator +"results"+File.separator+ "temp");
				if (tempDir.exists())
					deleteDirectory(tempDir);
			}
		}
		return result;
	}


	/**
	 * Parses the interactions in a String that will be given as a command argument to the python scripts.
	 * @return String containing the information on interactions that will be given as a command argument to the python scripts
	 */
	private String interactionsToString() {
		String interactions = "{";
		Iterator<String> iter = interactionsMap.keySet().iterator();
		String key;
		while (iter.hasNext()) {
			key = iter.next();
			interactions += "\"" + key + "\":\"" +
					interactionsMap.get(key)
					+ "\", ";
		}
		interactions = interactions.substring(0, interactions.length() -
				2);
		interactions += "}";
		return interactions;
	}

	/**
	 * Deletes the directory given by the dir argument, typically the temp directory created during execution.
	 * @param dir : the directory to be suppressed.
	 */
	private void deleteDirectory(File dir){
		File [] files = dir.listFiles();
		if(files!=null){
			for(File f: files){
				if(f.isDirectory())
					deleteDirectory(f);
				else
					f.delete();
			}
		}
		dir.delete();
	}

	/**
	 * This function extracts a file located in pathToZip,copies it in directory, with a new name and then unzips it.
	 * @param directory : the place where the new file is to be written
	 * @param pathToZip : the place where to find the file (within the jar archive)
	 * @param name : the new file name
	 * @throws Exception
	 */
	private void extractAndUnzip(File directory, String pathToZip, String name) throws Exception {
		InputStream in = getClass().getResourceAsStream(pathToZip);
		File zip = new File(directory.getAbsolutePath() +File.separatorChar+ name);
		BufferedInputStream bufIn = new BufferedInputStream(in);
		BufferedOutputStream bufOut = new BufferedOutputStream(new FileOutputStream(zip));

		byte[] inByte = new byte[4096];
		int count = bufIn.read(inByte);
		while (count != -1) {
			bufOut.write(inByte, 0, count);
			count=bufIn.read(inByte);
		}
		bufIn.close();
		bufOut.close();

		zipExe=zip;

		String[] cmd_unzip =new String[4];
		cmd_unzip[0]="unzip";
		cmd_unzip[1]=zipExe.getAbsolutePath();
		cmd_unzip[2]="-d";
		cmd_unzip[3]=directory.getAbsolutePath()+File.separatorChar+"check_inflgraph_11";
		Runtime rt = Runtime.getRuntime();
		Process pr_unzip=rt.exec(cmd_unzip);
		pr_unzip.waitFor();



	}
	public void setNodeBorderPos(Color c){
		nodeBorderPos=c;
	}
	public void setNodeBorderNeg(Color c){
		nodeBorderNeg=c;
	}
	public void setNodeColorPos(Color c){
		nodeColorPos=c;
	}
	public void setNodeColorNeg(Color c){
		nodeColorNeg=c;
	}
	public void setEdgesPos(Color c){
		edgesPos=c;
	}
	public void setEdgesNeg(Color c){
		edgesNeg=c;
	}
	public void setInputs(Color c){
		inputs=c;
	}


	public Color getNodeBorderPos(){
		return nodeBorderPos;
	}
	public Color getNodeBorderNeg(){
		return nodeBorderNeg;
	}
	public Color getNodeColorPos(){
		return nodeColorPos;
	}
	public Color getNodeColorNeg(){
		return nodeColorNeg;
	}
	public Color getEdgesPos(){
		return edgesPos;
	}
	public Color getEdgesNeg(){
		return edgesNeg;
	}
	public Color getInputs(){
		return inputs;
	}
	/**
	 * This function changes the visual style after the predictions have been made.
	 * <p>Node are painted green if they are observed present, red if they are observed absent.</p>
	 * <p>The border is wide if a prediction has been made.</p>
	 * <p>The border is green if the node is predicted present, red if it is predicted absent.</p>
	 */
	private void setVisualStyle() {
		VisualMappingManager vmm = adapter.getVisualMappingManager();
		// retrieves the current style rules and functions in the network
		VisualStyle currentStyle = vmm.getCurrentVisualStyle();
		String plusChar = "+";
		String minusChar= "-";
		// we are now going to define some mapping rules

		/*----rule 1 : the nodes for which a prediction has been made, 
		 *------------- whatever the prediction is
		 *------------- will be drawn with a wide border */
		DiscreteMapping borderWidth = 
				(DiscreteMapping) 
				adapter.getVisualMappingFunctionDiscreteFactory()
				.createVisualMappingFunction(NEW_ATTRIBUTE_PREDICTION, 
						String.class, 
						BasicVisualLexicon.NODE_BORDER_WIDTH);
		Double value=8d;
		borderWidth.putMapValue(plusChar,value);
		borderWidth.putMapValue(minusChar,value);
		borderWidth.putMapValue("input",value);

		/*----rule 2 : the nodes for which a prediction has been made, 
		 *------------- will have a :
		 *------------- green border if they have a '+' prediction (possible to change this color in the color panel)
		 **-------------red border if they have a '-' prediction (possible to change this color in the color panel)
		 *--------------blue border if they have a "input" prediction (repair mode 4, possible to change this color in the color panel)*/
		DiscreteMapping borderPaint = 
				(DiscreteMapping) 
				adapter.getVisualMappingFunctionDiscreteFactory()
				.createVisualMappingFunction(NEW_ATTRIBUTE_PREDICTION, 
						String.class, 
						BasicVisualLexicon.NODE_BORDER_PAINT);
		borderPaint.putMapValue(plusChar,getNodeBorderPos());
		borderPaint.putMapValue(minusChar,getNodeBorderNeg());
		borderPaint.putMapValue("input",getInputs());


		/*----rule 3 : the nodes for which an observation exists, 
		 *------------- will be painted (inside):
		 *------------- green if they have a '+' observation (possible to change this color in the color panel)
		 **-------------red if they have a '-' observation (possible to change this color in the color panel)*/
		DiscreteMapping insidePaint = 
				(DiscreteMapping) 
				adapter.getVisualMappingFunctionDiscreteFactory()
				.createVisualMappingFunction(NEW_ATTRIBUTE_OBSERVATION, 
						String.class, 
						BasicVisualLexicon.NODE_FILL_COLOR);
		insidePaint.putMapValue(plusChar,getNodeColorPos());
		insidePaint.putMapValue(minusChar,getNodeColorNeg());


		/*----rule 4 : the shape of the edge arrow is a triangle in case of a positive interaction
		 * ------------it is a T if it is a negative interaction
		 **------------*/
		DiscreteMapping edgesTargets=(DiscreteMapping) 
				adapter.getVisualMappingFunctionDiscreteFactory()
				.createVisualMappingFunction(NEW_ATTRIBUTE_EDGE_SIGN, 
						String.class, 
						BasicVisualLexicon.EDGE_TARGET_ARROW_SHAPE);
		edgesTargets.putMapValue(plusChar, ArrowShapeVisualProperty.DELTA);
		edgesTargets.putMapValue(minusChar, ArrowShapeVisualProperty.T);



		// we add these rules to the set of rules we previously had
		currentStyle.removeVisualMappingFunction(BasicVisualLexicon.NODE_BORDER_WIDTH);
		currentStyle.addVisualMappingFunction(borderWidth);
		currentStyle.removeVisualMappingFunction(BasicVisualLexicon.NODE_BORDER_PAINT);
		currentStyle.addVisualMappingFunction(borderPaint);
		currentStyle.removeVisualMappingFunction(BasicVisualLexicon.NODE_FILL_COLOR);
		currentStyle.addVisualMappingFunction(insidePaint);
		currentStyle.removeVisualMappingFunction(BasicVisualLexicon.EDGE_TARGET_ARROW_SHAPE);
		currentStyle.addVisualMappingFunction(edgesTargets);
		currentStyle.removeVisualMappingFunction(BasicVisualLexicon.EDGE_UNSELECTED_PAINT);
		currentStyle.removeVisualMappingFunction(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);

		/*----rule 5 : the edge is painted according to the prediction made
		 * ------------if colors have been chosen for edge predictions
		 * ------------in case repair mode is flip influences (2)
		 **------------*/
		if (getEdgesPos()!=null || getEdgesNeg()!=null){
			DiscreteMapping edgesPaint=(DiscreteMapping) 
					adapter.getVisualMappingFunctionDiscreteFactory()
					.createVisualMappingFunction(NEW_ATTRIBUTE_PREDICTION, 
							String.class, 
							BasicVisualLexicon.EDGE_UNSELECTED_PAINT);
			DiscreteMapping edgesStrokePaint=(DiscreteMapping) 
					adapter.getVisualMappingFunctionDiscreteFactory()
					.createVisualMappingFunction(NEW_ATTRIBUTE_PREDICTION, 
							String.class, 
							BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);

			if (getEdgesPos()!=null){
				edgesPaint.putMapValue(plusChar,getEdgesPos());
				edgesStrokePaint.putMapValue(plusChar,getEdgesPos());
			}
			if (getEdgesNeg()!=null){
				edgesPaint.putMapValue(minusChar,getEdgesNeg());
				edgesStrokePaint.putMapValue(minusChar,getEdgesNeg());
			}

			currentStyle.addVisualMappingFunction(edgesPaint);
			currentStyle.addVisualMappingFunction(edgesStrokePaint);
		}

		// we redraw the networks
		Set<CyNetworkView> views= adapter.getCyNetworkViewManager().getNetworkViewSet();
		for (CyNetworkView view : views){
			try {
				currentStyle.apply(view);
			}
			catch (ConcurrentModificationException e) {
				System.out.println("ConcurrentModificationException caught while applying new style to a view");
			}
			try {
				view.updateView();
			}
			catch (ConcurrentModificationException e) {
				System.out.println("ConcurrentModificationException caught while updating view");
			}
		}
	}




	public String removeLines(String s){
		return s.replaceAll("\\n","\\\\n");
	}
	/**
	 * Stops the executions of the scripts.
	 * <p> Calls stopPython() for each net. </p>
	 * <p> Sets calculating to false and stopped to true.<p>
	 * @throws IOException
	 */
	public void stopCalculating() throws IOException {
		if (calculating) {
			calculating = false;
			stopped = true;
			for (CytoAspSingleNet n : nets) {
				n.stopPython();
			}
			pool.shutdown();
		}
	}


	/**
	 * Value contained in the calculating field.
	 * @return True if the calculation is ongoing, false otherwise.
	 */
	public boolean isCalculating(){
		return calculating;
	}

	private class CytoAspSingleNet implements Callable<String> {
		private CyNetwork net;
		private CySwingAppAdapter adapter;
		private String netName;
		/**
		 * This field will contain the different observations if the user decides to use the
		 * "observations from attribute" option
		 */
		private String observations = "";
		/**
		 * This field will contain the File containing the observations, 
		 * obtained either from a file or from the observations field.
		 */
		private File obsFile;
		boolean pythonRunning = false;
		private String timeValue="";

		/**
		 * Constructor CytoAspSingleNet
		 * @param net : the CyNetwork to put in the private field net
		 * @param adapter : the CySwingAppAdapter to use in this simple app
		 */
		public CytoAspSingleNet(CyNetwork net,CySwingAppAdapter adapter) {
			this.adapter=adapter;
			this.net = net;
			netName=net.getRow(net).get(CyNetwork.NAME, String.class);
			this.timeValue = (new Long(System.currentTimeMillis())).toString();
		}


		/**
		 * This function initialises the ObsFile field
		 * @param arg : the File containing the different observations to use.
		 * If null, this function will create a new File, and write the content of the observations field inside. 
		 * @throws IOException
		 */
		public void setObsFile(File arg) throws IOException {
			File resultDir=new File(directory.getAbsolutePath()+File.separatorChar+"results");
			if (!resultDir.exists()){
				resultDir.mkdir();
			}
			if (arg == null) {
				// observations from an attribute
				obsFile = new File(resultDir.getAbsolutePath()
						+ File.separatorChar + netName +
						".obs");
				BufferedWriter writer = new BufferedWriter(new
						FileWriter(obsFile));
				writer.write(observations);
				writer.close();
			} else {
				// observations from a file
				obsFile = new File(resultDir.getAbsolutePath() + File.separator +netName+ ".obs");
				Files.copy(arg.toPath(), obsFile.toPath(),StandardCopyOption.REPLACE_EXISTING);
			}
		}

		/**
		 * @return the CyNetwork contained in the field net
		 */
		public CyNetwork getNet() {
			return net;
		}

		/**
		 * Initialises the observations field
		 * @param arg : the String containing the different observations 
		 * that can be deduced from the "observations from attribute" option
		 */
		public void setObservations(String arg) {
			this.observations = arg;
		}
		/*This function used the built-in cytoscape function to export a network to sif format. 
		 * However, this was a problem whenever an isolated node existed.
		 * That's why it's been replaced by the following method in the code.

		/**
		 * Writes the network in a sif format to a networkname.sif file in the directory provided
		 * @param directory : a File giving the destination location
		 * @return the name of the file created
		 * @throws Exception

		private String exportNetworkToSif2(File directory) throws
		Exception {
			File netFile = new File(directory.getAbsolutePath()
					+ File.separatorChar + netName + ".sif");
			CyNetworkViewWriterManager VWM=adapter.getCyNetworkViewWriterManager();
			List<CyFileFilter> CFF = VWM.getAvailableWriterFilters();
			for (CyFileFilter filter : CFF) {
				for (String ext : filter.getExtensions()){
					if (ext.equals("sif")) {
						Task writer = VWM.getWriter(net, filter, netFile);
						adapter.getTaskManager().execute(new TaskIterator(writer));
						return netFile.getAbsolutePath();
					}
				}
			}
			return "";
		}
		 */
		/**
		 * Writes the network in a sif format to a networkname.sif file in the directory provided
		 * @param directory : a File giving the destination location
		 * @return the name of the file created
		 * @throws Exception
		 */
		private String exportNetworkToSif(File directory) throws
		Exception {
			String networkSif="";
			Boolean edgesWrong=false;
			for (CyEdge edge:net.getEdgeList()){
				String source=removeLines(net.getRow(edge.getSource()).get(CyNetwork.NAME, String.class));
				String target=removeLines(net.getRow(edge.getTarget()).get(CyNetwork.NAME, String.class));
				String interaction=net.getRow(edge).get("interaction", String.class);
				if (interaction==null || interaction.equals("")){
					interaction="-";
				}
				if ((source!=null) && (target!=null) && (source!="") && (source!="")){
					networkSif+=source+"\t"+interaction+"\t"+target+"\n";
				}
				else {
					edgesWrong=true;
				}
			}


			File netFile = new File(directory.getAbsolutePath()
					+ File.separatorChar + netName + ".sif");
			BufferedWriter writer = new BufferedWriter(new
					FileWriter(netFile));
			writer.write(networkSif);
			writer.close();
			if (edgesWrong){
				System.out.println("Some edges were ignored because they do not contain source or target nodes.");
			}
			return netFile.getAbsolutePath();
		}

		/* (non-Javadoc)
		 * this function executes the executable check_inflgraph_11 via a script command cmd
		 * cmd contains the different options that are to be given to the python script
		 * once the command is executed, this function fills the 
		 * NEW_ATTRIBUTE_PREDICTION and NEW_ATTRIBUTE_OBSERVATIONS columns of nodes attributes
		 * these attributes are then used by setObservationsFromAttribute(String attr) to 
		 * change the visual style in cytoscape 
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public String call() throws Exception {
			File resultDir=new File(directory.getAbsolutePath()+File.separatorChar+"results");
			String sifFile = exportNetworkToSif(resultDir);
			String[] cmd = new String[9];
			cmd[0] =directory.getAbsolutePath()+File.separatorChar+"check_inflgraph_11"+File.separatorChar+"exe.linux-x86_64-2.7"+File.separatorChar+"check_inflgraph_11";
			cmd[1] = sifFile;
			cmd[2] = obsFile.getAbsolutePath();
			cmd[3] = "" + repairMode;
			if (mics)
				cmd[4] = "Y";
			else
				cmd[4] = "N";
			if (repairSets)
				cmd[5] = "Y";
			else
				cmd[5] = "N";
			cmd[6] = "N"; // the temp directory will be deleted from java, but
			// the option should stay in python script
			cmd[7] = interactionsToString();
			cmd[8] = timeValue;
			Runtime rt = Runtime.getRuntime();
			String result = netName + ":\n";
			String line = "";
			try {// execution of python script
				pythonRunning = true;
				Process pr = rt.exec(cmd);

				BufferedReader bfr = new BufferedReader(new
						InputStreamReader(
								pr.getInputStream()));
				while ((line = bfr.readLine()) != null) {
					result += line + "\n";
				}
			} catch (IOException e) {
				result += "Error while executing check_inflgraph: "
						+ e.getMessage();
			}
			pythonRunning = false;
			if (!stopped) {
				/* if the computation is over and the program hasn't been stopped,
				 *  this part reads the results provided by python and fills the columns of attributes
				 *  that cytoscape will then use with the function setObservationsFromAttribute(String attr)
				 *  to change styles according to the observations available and the predictions made
				 */
				File predictionsFile = new File(directory.getAbsolutePath()
						+ File.separatorChar + "results" + File.separatorChar
						+ netName + "-pred.txt");

				Map<String, String> predictionsMap = new TreeMap<String, String>();
				Map<String, String> observationsMap = new TreeMap<String, String>();
				Map<String, Character> edgesPredictionsMap = new TreeMap<String, Character>();
				String[] lineArray;

				//reads the observations given from the obsFile and fills in observationsMap accordingly
				BufferedReader in = new BufferedReader(new FileReader(obsFile));
				while ((line = in.readLine()) != null) {
					lineArray = null;
					if (line.contains("="))
						lineArray = line.split("=");
					else if (line.contains("\t"))
						lineArray = line.split("\t");
					if (lineArray != null)
						observationsMap.put(lineArray[0].trim(),
								lineArray[1].trim());
				}
				in.close();
				//reads the predictions made from the predictionsFile and fills in predictionsMap and edgesPredictionsMap accordingly
				in = new BufferedReader(new
						FileReader(predictionsFile));
				while ((line = in.readLine()) != null) {
					lineArray = null;
					if (line.contains("=")) {
						lineArray = line.split("=");
						predictionsMap.put(lineArray[0].trim(),
								lineArray[1].trim());
					}
					if (line.contains("->")) {
						Character sign=line.charAt(line.length()-1);
						String edge= line.substring(0,line.length()-2);
						edgesPredictionsMap.put(edge, sign);
					}
				}
				in.close();
				//fills in the NEW_ATTRIBUTE_PREDICTION and NEW_ATTRIBUTE_OBSERVATIONS columns of attributes
				//from the information found in predictionsMap and observationsMap
				List<CyNode> nodeList = net.getNodeList();
				for (CyNode node : nodeList)  {
					CyRow row=net.getRow(node);
					if (predictionsMap.containsKey(removeLines(net.getRow(node).get(CyNetwork.NAME, String.class).toString()))) {
						String predictedValue=predictionsMap.get(removeLines(net.getRow(node).get(CyNetwork.NAME, String.class)));
						row.set(NEW_ATTRIBUTE_PREDICTION, predictedValue);
					}
					else {
						row.set(NEW_ATTRIBUTE_PREDICTION, "");
					}
					if (observationsMap.containsKey(removeLines(net.getRow(node).get(CyNetwork.NAME, String.class).toString()))) {
						String observedValue=observationsMap.get(removeLines(net.getRow(node).get(CyNetwork.NAME, String.class)));
						row.set(NEW_ATTRIBUTE_OBSERVATION, observedValue);
					}
					else {
						row.set(NEW_ATTRIBUTE_OBSERVATION, "");
					}
				}

				//fills in the NEW_ATTRIBUTE_PREDICTION columns of attributes
				//from the information found in edgesPredictionMaps
				List<CyEdge> edgeList = net.getEdgeList();

				for (CyEdge edge:edgeList){
					CyRow row=net.getRow(edge);
					String nodeCouple="";
					nodeCouple=removeLines(net.getRow(edge.getSource()).get(CyNetwork.NAME, String.class))+" -> ";
					nodeCouple+=removeLines(net.getRow(edge.getTarget()).get(CyNetwork.NAME, String.class));
					if (edgesPredictionsMap.containsKey(nodeCouple)){
						Character predictedValue=edgesPredictionsMap.get(nodeCouple);
						row.set(NEW_ATTRIBUTE_PREDICTION, predictedValue.toString());
					}
					else {
						row.set(NEW_ATTRIBUTE_PREDICTION, "");
					}
				}
				//computes prediction for input nodes in case repair mode "input nodes" is chosen. It computes the intersection of all repair sets and 
				//add "input" in the column prediction if the node is in the intersection
				if (repairMode==3){
					File repairFile = new File(directory.getAbsolutePath()
							+ File.separatorChar + "results" + File.separatorChar
							+ netName + "-repair.txt");
					if (repairFile.exists()){
						in = new BufferedReader(new
								FileReader(repairFile));
						Boolean filled=false;
						LinkedList<String> inputsPrediction=new LinkedList<String>();
						LinkedList<String> inputsPrediction2=new LinkedList<String>();
						LinkedList<String> prov=new LinkedList<String>();
						while ((line = in.readLine()) != null) {
							if (line.contains("repair")&&!line.contains("repair 1:")&&filled){//end of a repair set, not the first one
								for (String node :inputsPrediction){
									if (prov.contains(node)){
										inputsPrediction2.add(node);
									}
								}
								inputsPrediction=(LinkedList<String>) inputsPrediction2.clone();
								inputsPrediction2=new LinkedList<String>();
								prov=new LinkedList<String>();
							}
							else if (line.contains("repair")&&!line.contains("1")&&!filled){//end of the first repair set, beginning of the second one
								filled=true;
							}
							else if (!filled){
								if (line.contains("Define")){
									inputsPrediction.add(line.substring(7,line.length()-9).trim());//removes "Define " and " as input" and keeps the node name
								}
							}
							else if (filled){
								if (line.contains("Define")){
									prov.add(line.substring(7,line.length()-9).trim());
								}
							}
						}
						for (String node :inputsPrediction){
							if (prov.contains(node)){
								inputsPrediction2.add(node);
							}
						}
						inputsPrediction=(LinkedList<String>) inputsPrediction2.clone();
						for (CyNode node : nodeList)  {
							CyRow row=net.getRow(node);
							if (inputsPrediction.contains(removeLines(row.get(CyNetwork.NAME, String.class)).toString())){
								row.set(NEW_ATTRIBUTE_PREDICTION, "input");
							}
						}
						
					}
				}
			}
			return result;
		}

		/**
		 * Stops the execution of the python script and sets pythonRunning to False
		 * @throws IOException
		 */
		public void stopPython() throws IOException {
			if (pythonRunning) {
				String pid;
				BufferedReader inReader = new BufferedReader(
						new FileReader(
								new File(directory.getAbsolutePath()
										+ File.separator + "temp" + File.separator
										+ netName + "-pid"+timeValue+".txt")));
				if ((pid = inReader.readLine()) != null) {
					Runtime.getRuntime().exec("pkill -P " + pid);
				}
				pythonRunning = false;
				inReader.close();
			}
		}
	}
}
