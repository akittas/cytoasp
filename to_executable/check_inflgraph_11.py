# -*- coding: utf-8 -*-
from  pyasp.asp import *

from __ingranalyze__ import query as pm
from __ingranalyze__ import bioquali, utils

import sys
import os
#from bioasp.misc import *
import tempfile
#from bioasp.data import op
import netio
from cStringIO import StringIO
import shutil
from functools import  partial
import re
import string

def encodeObsFile(obs_file, nodeLegends, pathToDir, pathToRes):
    nodesAbsent = False
    newfileName = os.path.splitext(os.path.split(obs_file)[1])
    newfile = pathToDir + newfileName[0]+"-enc"+newfileName[1]
    absNodesString = ''
    absNodesFile = pathToRes + newfileName[0] +"-net_abs"+newfileName[1]
    with open(obs_file, 'r') as mapto, open(newfile, 'w') as outfile:
        firstline=mapto.readline()
        if '\t' in firstline or '=' in firstline:
            firstline = ""
        for line in mapto:
            words = re.split(r"\t| = ", line)
            if words[0].strip() in nodeLegends:
                words = [w.strip() for w in words]
                words[0] = nodeLegends[words[0]]
                outfile.write(" = ".join(words) + "\n")
            else:
                absNodesString += line
                nodesAbsent = True

    if(nodesAbsent):
        with open(absNodesFile, 'w') as absFile:
            absFile.write(absNodesString)
        print("Some nodes in the observation file don't exist in the network and therefore were not checked for consistency. The list of these nodes is saved in the file " + absNodesFile) 
    return newfile, firstline


def print_predictions(filename, predictions, nodeLegends) :
    predictions = sorted(predictions, key=lambda p: str(p.arg(0)))
    with open (filename, "w") as pf:
        pf.write ('Predictions\n')
        for p in predictions:
            if p.pred() == "vlabel" :
                pf.write(nodeLegends[getNodeName(str(p.arg(1)))] + ' = ')
                if p.arg(2) == "-1": pf.write('-\n')
                elif p.arg(2) == "1" : pf.write('+\n')
                elif p.arg(2) == "0" : pf.write('nc\n')
            if p.pred() == "elabel" :
                pf.write(nodeLegends[getNodeName(str(p.arg(0)))] + ' -> ' + nodeLegends[getNodeName(str(p.arg(1)))])
                if p.arg(2) == "-1" : pf.write(' -\n')
                elif p.arg(2) == "1"  : pf.write(' +\n')           
    
    print('Predictions are saved in'+filename)       

def print_mic(nodeLegends, mic, net, obs):
    micStr = StringIO()
    nodes = []
    edges = []
    for node in mic: nodes.append(str(node.arg(1)))
    predecessors = []
    for e in net:
        if e.pred() == "obs_elabel" :
            if str(e.arg(1)) in nodes : 
                predecessors.append(str(e.arg(0)))
                edges.append(nodeLegends[getNodeName(str(e.arg(0)))] + " -> " + nodeLegends[getNodeName(str(e.arg(1)))])
                if str(e.arg(2)) == "1" :  edges.append(" +\n")
                elif str(e.arg(2)) == "-1" : edges.append(" -\n")

    for edge in edges: micStr.write(edge)
    for o in obs:
        if o.pred() == "obs_vlabel" :  
            if str(o.arg(1)) in nodes :
                if str(o.arg(2)) == "1" :  micStr.write(nodeLegends[getNodeName(str(o.arg(1)))] + " = +\n")
                elif str(o.arg(2)) == "-1" :  micStr.write(nodeLegends[getNodeName(str(o.arg(1)))] + " = -\n")
            if str(o.arg(1)) in predecessors :
                if str(o.arg(2)) == "1" :  micStr.write(nodeLegends[getNodeName(str(o.arg(1)))] + " = +\n")
                elif str(o.arg(2)) == "-1" :  micStr.write(nodeLegends[getNodeName(str(o.arg(1)))] + " = -\n")

    return micStr.getvalue()

def clean_up() :
    if os.path.isfile("parser.out"): os.remove("parser.out")
    if os.path.isfile("asp_py_lextab.py"): os.remove("asp_py_lextab.py")
    if os.path.isfile("asp_py_lextab.pyc"): os.remove("asp_py_lextab.pyc")
    if os.path.isfile("asp_py_parsetab.py"): os.remove("asp_py_parsetab.py")
    if os.path.isfile("asp_py_parsetab.pyc"): os.remove("asp_py_parsetab.pyc") 
    if os.path.isfile("graph_parser_lextab.py"): os.remove("graph_parser_lextab.py")
    if os.path.isfile("graph_parser_parsetab.py"): os.remove("graph_parser_parsetab.py")

def calculateMics(micFile, net, nodeLegends, netWOData, mu = []):
    try: 
        mics = pm.get_minimal_inconsistent_cores(net)
        count = 1
        oldmic = 0
        with open(micFile, 'w') as mf:
            for mic in mics:
                if oldmic != mic: 
                    mf.write( 'mic ' + str(count) + ':\n')
                    mf.write(print_mic(nodeLegends, mic.to_list(), netWOData.to_list(), mu.to_list()))
                    count += 1
                    oldmic = mic
        print('There are '+str(count-1)+' MICs. They are saved in '+micFile)    
    except Exception as err:
        print ("Error during calculating minimum inconsistent cores: "+err.strerror)



def getNodeName(changedName):
    return changedName[5:-2]

def noderepl(matchobj, nodelegends):
    return nodelegends[getNodeName(matchobj.group(0))]

def changeNodeNames(strLine, nodelegends):
    newLine=re.sub('gen\(\"[a-zA-Z0-9]+\"\)',partial(noderepl, nodelegends = nodelegends), strLine)
    return newLine

def getRepairOptions(network, repairchoice):
    repair_options= TermSet()
    if repairchoice=="1":
        print 'repair mode: flip observed variations ...',
        repair_options = pm.get_repair_options_flip_obs(network)
    if repairchoice=="2":
        print 'repair mode: flip influences ...',                   
        repair_options = pm.get_repair_options_flip_edge(network)
    if repairchoice=="3":
        print 'repair mode: define network nodes as inputs ...',                 
        repair_options = pm.get_repair_options_make_node_input(network)
    if repairchoice=="4":
        print 'repair mode: define network nodes as input in an experiment ...'                    
        repair_options = pm.get_repair_options_make_obs_input(network)
    if repairchoice=="5":
        print 'repair mode: add influence ...',               
        repair_options = pm.get_repair_options_add_edges(network)
    optimum = pm.get_minimum_of_repairs(network, repair_options)    # previously optimum = pm.get_minimum_of_repairs(net_with_data, repair_options)  
    return repair_options, optimum

def getRepairs(network, repair_options, optimum, repairfile, nodelegend, emptyNetwork=True):   
    models=TermSet()
    if emptyNetwork :
        models = pm.get_minimal_repair_sets(network, repair_options, optimum[0])
    else :
        models = pm.get_minimal_repair_sets(network, repair_options, optimum.score[0])
    
    count = 1
    oldmodel = 0
    with open (repairfile, "w") as rf:
        for model in models:
            if oldmodel != model: 
                oldmodel = model
                repairs = model.to_list()
                rf.write( "repair " + str(count)+':\n')
                for r in repairs : 
                    line = changeNodeNames(str(r.arg(0)), nodelegend)
                    words = line.rsplit(",", 2)
                    if words[0].find("vflip") != -1:
                        rf.write(words[1]+" = ")
                        if words[2][:-1]=="1" :
                            rf.write("-\n")
                        else:
                            rf.write("+\n") 
                    elif words[0].find("eflip") != -1:
                        rf.write(words[0][6:]+' -> '+words[1]+' ')
                        if words[2][:-1]== "1" :
                            rf.write("-\n")
                        else:
                            rf.write("+\n") 
                    elif words[0].find("ivert") != -1:
                        rf.write('Define ' )
                        if line.find(",") != -1: #option4 
                            rf.write(words[-1][:-1])                     
                        else: 
                            rf.write(words[0][6:-1])
                        rf.write(' as input\n')
                    elif words[0].find("aedge") != -1:
                        rf.write(words[0][6:] + ' -> '+words[1][:-1]+'\n')
                    else:
                        rf.write(line+'\n')
                count+=1
                rf.write('\n')
    print('There are '+str(count-1)+' repair sets. They are saved in '+repairfile)

"""
Parameters:
1 - network file in sif format
2 - file with observations
3 - repair mode, following options are possible:
  1 - flip observed variations, 2 - flip influences, 3 - define nodes as input, 
  4 - define observed variations as input, 5 - add influences
4 - if the mics are to be computed, Y- for yes
5 - if possible repairs sets are to be computed, Y for yes
6-  if the folder with help files is to be deleted, Y for  yes
7 - dictionary for the interactions. Format:  {"agent":"+", "output":"+", ..., "inhibitor":"-"}
8 - unique timestamp for the file with process id.
"""    

if len(sys.argv) == 9 :
    net_string = sys.argv[1]
    obs_string = sys.argv[2]
    """"repair mode, following options are possible:
  1 - flip observed variations, 2 - flip influences, 3 - define nodes as input, 
  4 - define observed variations as input, 5 - add influences"""
    repairchoice = sys.argv[3] 
    if not (repairchoice in ["1", "2", "3", "4", "5"]) : print "ERROR in the choice of repair mode !", exit(0)
    do_mics = sys.argv[4]
    do_repair = sys.argv[5]
    del_helpfolder = sys.argv[6]
    signs = sys.argv[7]
    timeValue = sys.argv[8]

else:
    print("\nmissing arguments !!!")
    exit(0) 
try:
    
    pathToObs = os.path.split(net_string)[0]
    pathToRes = pathToObs +"/"
    net_filename = os.path.split(os.path.splitext(net_string)[0])[1]
    
    junkDir = pathToObs + "/"+"temp/"
    try :
        if not os.path.exists(pathToRes):
            os.mkdir(pathToRes)
    except Exception:
        pass
    try :
        if not os.path.exists(junkDir):
            os.mkdir(junkDir) 
    except Exception :
        pass
    with open (junkDir+net_filename + "-pid"+str(timeValue)+".txt", "w") as pr:
        pr.write(str(os.getpid()))       
    
    nodeLegends = netio.convert_sif_to_bq(net_string, junkDir, signs) [5]
    obsEncFile, expName = encodeObsFile(obs_string, nodeLegends, junkDir, pathToRes)
    os.chdir(junkDir)
    mu = bioquali.readProfile(obsEncFile)

    net = bioquali.readGraph(junkDir+net_filename + "-bqenc.net")

    predictionsFile = pathToRes + net_filename + "-pred.txt"
    repairFile = pathToRes + net_filename +"-repair.txt"
    invNodeLegend = dict((v,k) for k, v in nodeLegends.iteritems())
    inputs = pm.guess_inputs(net)
    consistent = pm.is_consistent(net)  
    if consistent: print( 'The empty network is consistent.')
    else:
        print('The empty network is inconsistent.')
        empty_net = net.union(inputs)
        consistent = pm.is_consistent(empty_net)
        if consistent: print('The empty network with input nodes is consistent.')
        else:
            print('The empty network with input nodes is still inconsistent.')    
            if do_mics == "Y":
                micFile = pathToRes + net_filename + "_mic.txt"  
                calculateMics(micFile, empty_net, invNodeLegend, net)
            repair_options, optimum = getRepairOptions(empty_net, repairchoice)  #problem here
            if do_repair == "Y":
                getRepairs(empty_net, repair_options, optimum, repairFile, invNodeLegend)
                #order of the arguments??
            # previously : model = pm.get_predictions_under_minimal_repair(optimum[0], empty_net, repair_options)
            print 'Computing predictions that hold under all repair sets size', optimum[0]
            model = pm.get_predictions_under_minimal_repair(empty_net, repair_options, optimum[0])

            
            
    if consistent: #if network is consistent add data
        net_with_data = net.union(mu).union(inputs)
        consistent = pm.is_consistent(net_with_data)
        if consistent: 
            print ('The network and data are consistent.')
            model = pm.get_predictions_under_consistency(net_with_data)     
        else:
            print('The network and the data are inconsistent.')
            if do_mics=="Y": 
                micFile = pathToRes + net_filename + "_mic.txt"   
                calculateMics(micFile, net_with_data,invNodeLegend, net, mu) 
            repair_options, optimum = getRepairOptions(net_with_data, repairchoice)
            print '   The data set can be repaired with minimal', optimum.score[0],'operations.'
            if do_repair == "Y":
                print 'Computing all repair sets with size', optimum.score[0]
                getRepairs(net_with_data, repair_options, optimum, repairFile, invNodeLegend,False)
            print 'Computing predictions that hold under all repair sets size', optimum.score[0]
            model = pm.get_predictions_under_minimal_repair(net_with_data, repair_options, optimum.score[0])
    predictions = model.to_list()
    print ( str(len(predictions)) + ' predictions found.')
    print_predictions(predictionsFile, predictions, invNodeLegend)
    #clean_up()     #creates problems when two scripts are run at the same time...
except Exception as err:
    print ("Error: ")
    print (err.strerror)
if del_helpfolder =="Y":
    try: 
        shutil.rmtree(junkDir) 
    except OSError:
        print ("Error while deleting directory")
