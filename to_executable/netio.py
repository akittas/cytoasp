'''
@author: A. Kittas 
@version 1.1, by J. Sereshti, signsString was added as parameter to the function convert_sif_to_bq  
Input/Output function for various network formats
'''
import os
import ast

def convert_sif_to_bq(sif_file, dir, signsString):
    '''Converts and outputs sif file to a bioquali format file, outputs also encoded formatting with legends'''
    sif_file_name = os.path.split(os.path.splitext(sif_file)[0])[1]
    bq_file = dir + sif_file_name + "-bq.net"  # e.g. Q12913|active -> pid_i_209291 +
    bqenc_file = dir +sif_file_name + "-bqenc.net"  # e.g. node1 -> node2 +
    nodelegend_file = dir +sif_file_name + "-nodelegend.noa"  # node legend, e.g. Q12913|active    node1
    edgelegend_file = dir +sif_file_name + "-edgelegend.eda"  # edge legend, e.g. node1    agent    node2
    edgeatt_file = dir +sif_file_name + "-edgesigns.eda"  # edge attributes for signs e.g Q12913|active (agent) pid_i_209291 = +
   
    nodes = {}
    signs = ast.literal_eval(signsString)

    with open(sif_file, 'r') as sif:
        sif_text = sif.read()

    nodenum = 0
    for line in sif_text.splitlines():  # make legend for nodes
        v = line.rstrip().split('\t')
        if not v[0] in nodes:
            nodes[v[0]] = 'node' + str(nodenum)
            nodenum += 1
        if not v[2] in nodes:
            nodes[v[2]] = 'node' + str(nodenum)
            nodenum += 1

    with open(nodelegend_file, 'w') as nodelegend:  # write nodes legend
        nodeLegends = dict()
        for k, v in nodes.items():
            nodelegend.write(k + '\t' + v + '\n')
            nodeLegends [k] = v

    # write edges legend, edge attribute signs, bq and bq-encoded format files
    with open(edgelegend_file, 'w') as edgelegend, open(edgeatt_file, 'w') as edgeatt, open(bq_file, 'w') as bq, open(bqenc_file, 'w') as bqenc:
        for line in sif_text.splitlines():
            v = line.rstrip().split('\t')
            edgelegend.write(nodes[v[0]] + '\t' + v[1] + '\t' + nodes[v[2]] + '\n')
            edgeatt.write(v[0] + ' (' + v[1] + ') ' + v[2] + ' = ' + (signs[v[1]] if v[1] in signs else '?') + '\n')
            bq.write(v[0] + ' -> ' + v[2] + ' ' + (signs[v[1]] if v[1] in signs else '?') + '\n')
            bqenc.write(nodes[v[0]] + ' -> ' + nodes[v[2]] + ' ' + (signs[v[1]] if v[1] in signs else '?') + '\n')

    return (bq_file, bqenc_file, nodelegend_file, edgelegend_file, edgeatt_file, nodeLegends)


def convert_bq_to_sif(bq_file, nodelegend_file, edgelegend_file):
    '''Converts and outputs a bioquali format encoded file to a sif file, needs corresponding edge and node legends'''
    sif_file = os.path.splitext(bq_file)[0] + "-sifdec.sif"
    with open(nodelegend_file, 'r') as nodelegend, open(edgelegend_file, 'r') as edgelegend:  # build dictionaries
        nodes = {}
        edges = {}
        for line in nodelegend:  # read nodes legend
            v = line.rstrip().split('\t')
            if len(v) > 1: nodes[v[1]] = v[0]  # encoded node names are keys
        for line in edgelegend:  # read edges legend
            v = line.rstrip().split('\t')
            if len(v) > 1:
                edges[v[0] + '\t' + v[2]] = v[1]  # node1\tnode2 are keys

    with open(bq_file, 'r') as bq, open(sif_file, 'w') as sif:
        for line in bq:
            v = line.rstrip().split(' -> ')
            if len(v) > 1:
                v[1] = v[1].split(' ')[0]
                sif.write(nodes[v[0]] + '\t' + edges[v[0] + '\t' + v[1]] + '\t' + nodes[v[1]] + '\n')

    return sif_file
