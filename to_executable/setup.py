from cx_Freeze import setup, Executable
#we have to add the following line to the query.py files in __ingranalyze__
#root = __file__.rsplit('/', 3)[0]+'/__ingranalyze__'

#we have to add the following line to the asp.py files in pyasp
#root = __file__.rsplit('/', 3)[0]+'/pyasp'

packages=["__ingranalyze__","pyasp"]

includefiles=[("/nms/st/k1347505/.local/lib64/python2.7/site-packages/pyasp/bin/gringo","pyasp/bin/gringo"),
              ("/nms/st/k1347505/.local/lib64/python2.7/site-packages/pyasp/bin/clasp","pyasp/bin/clasp"),
              ("/nms/st/k1347505/.local/lib64/python2.7/site-packages/pyasp/bin/hclasp","pyasp/bin/hclasp"),
              ("/nms/st/k1347505/.local/lib64/python2.7/site-packages/__ingranalyze__/encodings/","__ingranalyze__/encodings/")]

build_exe_options={"packages":packages,"include_files": includefiles}
setup(
    name = "check_inflgraph",
    version = "0.1",
    options={"build_exe" : build_exe_options},
    description = "this program checks the consistency of the network provided",
    executables = [Executable("check_inflgraph_11.py")],
)